---
title: Type de données abstrait
---

???+ note "Prérequis"

    On utilise la programmation orientée objet dans ce cours. Une présentation de ce paradigme de programmation est proposée dans [ce cours](../../3_langage/2_paradigmes/index.md).

L'informatique est la manipulation de données numériques, un *computer* est un *calculateur* (*computare* en latin : compter). Face à l'explosion des données disponibles il est indispensable de les présenter et les organiser correctement. 

Un exemple pour s'en rendre compte : vous avez devant vous vos manuels du lycée et vous devez traiter l'exercice de mathématiques 12 p. 58. Comment faites-vous ?

* vous parcourez la pile de livre depuis le premier jusqu'à trouver le bon livre ;
* vous feuilletez ce livre jusqu'à trouver la page 58 ;
* vous lisez les titres des exercices jusqu'à trouver le numéro 12.

Cela prend un peut moins d'une minute sans doute.

Maintenant faisons la même chose mais en considérant l'ensemble du CDI ! Il est hors de question de parcourir l'ensemble des étagères dans l'ordre jusqu'à trouver le bon livre... Heureusement, les livres/données sont organisés : chaque livre est classé selon son auteur, son thème... et les rayonnages sont organisés par thème et auteur.

Pour décrire cette situation, on parle en informatique de **type de données abstrait**

![Abstract Data Types](adt.jpg){width=35% .center .autolight}

???+ info "Type de données abstrait"

    Un type de données abstrait (*TDA*) est un modèle mathématique permettant de décrire des données. Afin de simplifier les choses, retenons qu'un *TDA* :

    * décrit les attributs d'un objet ;
    
    * décrit les opérations réalisables sur cet objet. Ces opérations sont définies à l'aide de **méthodes primitives**.

## Interface

???+ info "Interface"

    L'interface d'un *TDA* est sa spécification, la description de ses éléments constitutifs et de ses méthodes primitives. 
    
    On ne rentre pas dans le détail du code des méthodes, on se contente de donner leurs spécifications (entrées, rôle et sorties).
    
    ![Interface](iceberg.jpg){width=40% .center}
    
    Par exemple, une petite voiture de ville ou une berline luxueuse ont la même interface : un volant, un levier de vitesses... Par contre les détails sous-jacents diffèrent !

Prenons l'exemple d'une **liste chaînée**. Cette structure est décrite ci-dessous :

![Liste chaînée](liste_chainee.gif){width=65% .center .autolight}

Une liste chaînée est une structure de donnée permettant de stocker des *maillons*. La longueur de la liste n'est pas fixée *a priori* et peut évoluer au fil des ajouts et suppressions d'éléments.

Chaque maillon $x$ d'une liste chaînée comporte deux attributs :

* la valeur à stocker $x.donnee$
* un lien vers le prochain maillon $x.successeur$ : un maillon contient un autre maillon ! On peut ainsi se représenter la structure comme des éléments reliés le long d'une chaîne.

La liste chaînée $liste$ quant à elle contient au minimum un attribut, la **tête de la liste** $liste.tête$ qui est le premier élément (données + successeur). La liste vide est telle que sa tête contient `null` (elle pointe vers un élément nul).

Le lien du dernier élément de la liste vaut lui aussi `null` (il ne pointe nulle part).

Du point de vue des primitives, les méthodes minimales à implémenter sont :

* la création d'une liste vide ;

* l'insertion d'un élément (au début de la liste ici). On suppose que l'élément est non `null` ;

* la suppression du premier élément.

## Implantation

L'interface d'un type de données abstrait est son modèle théorique. Pour l'utiliser dans notre ordinateur, il faut le coder, l'**implanter** (*implémenter* est un anglicisme).

Le point important à retenir est qu'une interface peut avoir plusieurs implémentations différentes : le type de données utilisé, l'algorithme utilisé dans une méthodes peuvent varier d'un programmeur à l'autre.

Voici une implémentation de la liste chaînée en Python. Le mot clé `null` est remplacé par `#!py None`.

{{ IDE('listeChainee.py')}}

Certains points méritent d'être précisés :

* lors de la création, la liste est vide (la `tete` vaut `#!py None`) ;

* la méthode `est_vide` permet de savoir si la liste est ... vide ! C'est un classique des types abstraits de données. On l'utilise afin de vérifier que certaines opérations sont possibles (la suppression en particulier) ;

* l'insertion se fait en tête. On vérifie tout d'abord que le nouvel élément est bien un `Maillon` puis on modifie son successeur avant de « décaler » la tête de la liste ;

![Insertion](insertion.jpg){.autolight .center width=40%}

* la suppression de la valeur de tête commence par vérifier que la liste n'est pas vide (ce qui lèverait une erreur). Cette vérification faite, on décale la tête.

![Suppression en tête](suppression_tete.png){.autolight .center width=40%}
