---
title: Arbres binaires
---


## Arbre binaire

### Définitions

En première approche, on peut dire qu'un arbre **binaire** possède au maximum deux enfants. La définition exacte est toutefois un pue plus fine.

???+ info "Arbre binaire"

    Un arbre binaire est soit :
    
    * vide ;
    * soit formé d'un triplet $(racine~;~sag~;~sad)$ dans lequel :
        * $racine$ est un nœud ;
        * $sag$ est un arbre (éventuellement vide) appelé *sous-arbre gauche* ;
        * $sad$ est un arbre (éventuellement vide) appelé *sous-arbre droit*.

    <center>
    
    ```mermaid
    flowchart TD
    A((A))
    B((B))
    C((C))
    D((D))
    E((E))
    F[ ]
    G((G))
    H[ ]
    I[ ]
    J[ ]
    K[ ]
    L[ ]
    M[ ]
  
    A --- B
    A --- C
    B --- D
    B --- E
    C -.-x F
    C --- G
    D -.-x H
    D -.-x I
    E -.-x J
    E -.-x K
    G -.-x L
    G -.-x M

    style F fill:none, stroke:none
    style H fill:none, stroke:none
    style I fill:none, stroke:none
    style J fill:none, stroke:none
    style K fill:none, stroke:none
    style L fill:none, stroke:none
    style M fill:none, stroke:none
    ```
    </center>

    Dans la suite on ne représentera pas les arbres vides.

???+ note "Ordre"

    Dans un arbre binaire, les sous-arbres gauche et droit ont des rôles distincts, on ne peut pas les échanger. Les deux schémas ci-dessous ne représentent **pas** le même arbre.
    
    
    <center>
    
    ```mermaid
    flowchart TD
        A((A))
        B((B))
        C((C))
        D((A))
        E((B))
        F((C))
    
        A --- B
        A --- C
        D --- F
        D --- E
    ```
    </center>
    
Les arbres binaires sont utilisés si couramment en informatique que l'on a précisé certains cas particuliers.

???+ info "Arbre binaire parfait"

    Un arbre binaire est dit *parfait* si :
    
    * tous ses nœuds ont 0 ou 2 enfants ;
    * toutes ses feuilles sont situés à la même distance de la racine.

    De façon équivalent, on peut dire que tous les niveaux d'un arbre parfait sont entièrement remplis.

    <center>
    
    ```mermaid
    flowchart TD
    A((A))
    B((B))
    C((C))
    D((D))
    E((E))
    F((F))
    G((G))
  
    A --- B
    A --- C
    B --- D
    B --- E
    C --- F
    C --- G
    ```
    </center>

???+ note "Taille d'un arbre parfait"

    Un arbre parfait de hauteur $h$ compte $2^h-1$ nœuds.
    
    On peut s'en convaincre en numérotant les nœuds : la racine prend la valeur `1` puis, à chaque fois que l'on « descend » vers la gauche (resp. la droite), on ajoute un `0` (resp. un `1`) à la fin de l'étiquette.
    
    Ci-dessous, on constate que l'on a écrit les codes : `1`, `10`, `11`, `100`, `101`, `110`, `111`. Tous les nombres s'écrivant sur trois bits sauf le `0`. Il y en a $2^3-1$.
    
    <center>
    
    ```mermaid
    flowchart TD
    A(("<code>1</code>"))
    B(("<code>10</code>"))
    C(("<code>11</code>"))
    D(("<code>100</code>"))
    E(("<code>101</code>"))
    F(("<code>110</code>"))
    G(("<code>111</code>"))
  
    A --- B
    A --- C
    B --- D
    B --- E
    C --- F
    C --- G
    ```
    </center>
    
    Les plus « *matheux » noterons de façon analogue qu'il y a $1=2^0$ nœud au premier niveau, $2=2^1$ au deuxième, ..., $2^{h-1}$ au $h$-ième.
    
    Or la somme $1+2+4+...+2^{h-1}$ vaut $\dfrac{2^h-1}{2-1}=2^h-1$.
    

???+ note "Hauteur d'un arbre parfait"

    Un arbre parfait comptant $n$ nœuds a une hauteur égale à $\log_2\left(n+1\right)$.

    On rappelle que, si $x$ est un entier strictement positif, $\log_2\left(x\right) + 1$ est la taille de l'écriture de $x$ en binaire.
    
    Ainsi $\log_2\left(3\right) + 1 = 2$.
    
    
???+ info "Arbre binaire presque complet"

    Un arbre binaire est dit *presque complet* (ou *complet* mais cela peut être ambigu) si tous ses niveaux sont pleins sont éventuellement le dernier. 
    
    Si le dernier niveau est incomplet, les nœuds de ce niveaux sont *tassés* à gauche.
    
    <center>
    
    ```mermaid
    flowchart TD
    A((A))
    B((B))
    C((C))
    D((D))
    E((E))
    F((F))
    G[ ]
  
    A --- B
    A --- C
    B --- D
    B --- E
    C --- F
    C -.- G

    style G fill:none, stroke:none
    linkStyle 5 stroke:none;
    ```
    </center>

### Implantation

On propose ci-dessous une première mise en œuvre utilisant la programmation orientée objet. On crée deux classes afin de distinguer l'arbre vide des arbres non vides.

On a directement ajouté les méthodes permettant de calculer la taille et la hauteur de ces arbres et de les parcourir.

???+ info "Trois parcours en profondeur"

    Les arbres binaires possèdent trois parcours en profondeur :
    
    * le parcours *préfixe* (la racine est traitée avant les enfants) ;
    
    * le parcours *infixe* (la racine est traitée entre les enfants) ;
    
    * le parcours *suffixe* (la racine est traitée après les enfants).

{{ IDE('arbre_binaire_classe.py')}}

Une autre approche, qui n'utilise pas la programmation orientée objet, consiste à représenter :

* l'arbre vide par la valeur `#!py None`,
* un arbre non vide par un tuple `#!py (sag, valeur, sad)` dans lequel `sag` et `sad` sont eux-aussi des arbres.

{{ IDE('arbre_binaire_tuple.py')}}

???+ note "Cas des arbres presque complets"

    Les arbres presque-complets peuvent se mettre en œuvre astucieusement à l'aide d'un tableau.
    
    Si l'arbre compte $n$ nœuds, on considère un tableau de $n+1$ cellules.
    
    On laisse la cellule d'indice $0$ vide (ou on y place une valeur quelconque).
    
    La valeur de la racine est placée dans la cellule d'indice `1`.
    
    De façon générale, si la valeur d'un nœud est la cellule d'indice `i`, celle de son fils gauche est dans celle d'indice `#!py 2*i` et celle de son fils droit dans celle d'indice `#!py 2*i + 1`.
    
    L'arbre ci-dessous est représenté par : `#!py [None, "A", "B", "C", "D", "E", "F"]`.
    
    <center>
    
    ```mermaid
    flowchart TD
    A((A))
    B((B))
    C((C))
    D((D))
    E((E))
    F((F))
    G[ ]
  
    A --- B
    A --- C
    B --- D
    B --- E
    C --- F
    C -.- G

    style G fill:none, stroke:none
    linkStyle 5 stroke:none;
    ```
    </center>

## Arbre binaire de recherche

Un arbre binaire de recherche (*ABR*) est un type particulier d'arbre  binaire permettant de rechercher très efficacement des valeurs dans un ensemble.

### Définition

???+ info "Arbre binaire de recherche"

    Un arbre binaire de recherche (*ABR*) est un arbre binaire tel que :
    
    * toutes les valeurs des nœuds sont comparables ;
    * les valeurs de tous les nœuds situés dans le sous-arbre gauche sont *inférieures ou égales* à celle de la racine ;
    * les valeurs de tous les nœuds situés dans le sous-arbre droit sont *strictement supérieures* à celle de la racine.

    Dans la suite, on considérera que les valeurs dans le sous-arbre gauche sont **strictement** inférieures à celles de la racine. L'ABR ne possède donc pas de *doublons*.

    <center>
    
    ```mermaid
    flowchart TD
    A((15))
    B((13))
    C((19))
    D((11))
    E((14))
    F((16))
    G[ ]
  
    A --- B
    A --- C
    B --- D
    B --- E
    C --- F
    C -.- G

    style G fill:none, stroke:none
    linkStyle 5 stroke:none
    ```
    </center>
    
???+ note "Minimum et maximum, parcours"

    Dans un ABR, la valeur minimale est toujours portée par le nœud le plus à gauche. La valeur maximale est toujours portée par le nœud le plus à droite.
    
    De plus, le parcours infixe d'un ABR (sans doublons) parcourt les valeurs dans l'ordre croissant.
    
    <center>
    
    ```mermaid
    flowchart TD
    A((15))
    B((13))
    C((19))
    D((11))
    E((14))
    F((16))
    G[ ]
  
    A --- B
    A --- C
    B --- D
    B --- E
    C --- F
    C -.- G

    style G fill:none, stroke:none
    style D stroke:green,stroke-width:3px
    style C stroke:red,stroke-width:3px
    linkStyle 5 stroke:none
    ```
    </center>
    
    
### Implantation

On peut tout à fait reprendre et compléter l'implantation en programmation orientée objet utilisée pour les arbres binaires.

Il faut ajouter deux fonctions/méthodes :

* *insertion* : insère un nouveau nœud dans l'arbre à la première position convenable disponible ;
* *contient* : détermine si un nœud de l'arbre porte cette clé.


{{ IDE ('arbre_binaire_recherche_classe.py')}}

### Intérêt

Les *arbres binaires de recherches* servent à... chercher des éléments ! Il est donc intéressant de comparer leur complexité dans les opérations de recherches à celles d'autres structures de données.

Dans un tableau non ordonné, la recherche d'une clé nécessite au pire de lire toutes les valeurs : on est en complexité linéaire en la taille du tableau $\mathcal{O}\left( n\right)$.

Dans un *ABR*, les clés étant triées à chaque niveau, l'algorithme de recherche décide d'aller à gauche ou à droite à chaque étape selon la valeur rencontrée. Il n'a donc pas à lire toutes les clés.

Si l'arbre a une hauteur de $h$, il y aura donc au maximum $h$ étapes de recherche. Comme on a vu que, dans un arbre binaire, $h$ pouvait être calculé à partir de $\log_2(n)$, on est donc en complexité logarithmique (ce qui est meilleur qu'une complexité linéaire).

![Comparaison des temps de recherches](temps_recherche.png){width=70% .center .autolight}