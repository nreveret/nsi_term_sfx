---
title: Arbres
---

Un arbre est une structure de données permettant entre autres choses de décrire les relations hiérarchiques entre des objets.

<center>

```mermaid
flowchart TD
    A((A))
    B((B))
    C((C))
    D((D))
    E((E))
    F((F))
    G((G))
  
    A --- B
    A --- C
    A --- D
    B --- E 
    C --- F
    C --- G
```
</center>

On peut par exemple les utiliser afin de représenter des arbres de décision.

![Arbre de décision du médecin (simplifié !)](arbre_decision_medecin.png){width=60% .center .autolight}

## Arbre

Les arbres sont à l'intersection des mathématiques et de l'informatique : il est donc possible de les définir différemment selon le point de vue.

Aux yeux d'un mathématicien, un arbre est un [graphe *connexe*, *acyclique*](../4_graphes/index.md).  Les sommets du graphe sont appelés des *nœuds*.

L'informaticien précise cette définition en indiquant que l'un des nœud est la *racine* de l'arbre. 

Il est alors possible de définir les arbres récursivement. 

???+ info "Arbre enraciné"
    
    Un *arbre enraciné* (on parlera plus brièvement d'*arbre*) est un couple $(racine~;~enfants)$ dans lequel :

    * $racine$ est un nœud ;
    * $enfants$ est un ensemble d'arbres. Cet ensemble peut être vide.

    Si les nœuds contiennent des valeurs, on parle d'*arbre enraciné étiqueté*.

Cette définition est *récursive* car un enfant d'un arbre est lui-même un arbre (en pointillé ci-dessous).

![Arbre et sous-arbre](sous_arbres.png){.center .autolight width=40%}

???+ note "Pas d'ordre"

    Dans cette définition, l'ensemble des enfants n'est pas ordonné. Les deux schémas ci-dessous représentent **le même arbre**.
    
    <center>
    
    ```mermaid
    flowchart TD
        A((A))
        B((B))
        C((C))
        D((D))
    
        A --- B
        A --- C
        A --- D

        E((A))
        F((B))
        G((C))
        H((D))
    
        E --- H
        E --- G
        E --- F
    ```
    </center>

???+ info "Vocabulaire"
    
    Le vocabulaire associé est le suivant (entre parenthèse les traductions en anglais) :

    
    * la **taille** d'un arbre est son nombre de nœud ;

    * la **profondeur** (*level*) d'un nœud est sa position du nœud dans l'arbre. C'est la longueur du chemin entre la racine et le nœud. **Attention**, selon que l'on compte les arêtes ou les nœuds le long du chemin, la valeur peut varier ;
    
    * la **hauteur** d'un arbre est sa profondeur maximale ;
    
    * les **descendants** d'un nœud sont ses enfants et les enfants de ses enfants ;

    ![Vocabulaire](vocabulaire.png){.center .autolight width=60% align=right}
    * le **parent** (*parent*) d'un nœud est le nœud qui le précède directement dans l'arbre ;
    
    * les **ancêtres** (*ancestors*) d'un nœud sont son parents et les parents de ses parents ;
    
    * des nœuds de même parent sont **frères** (*siblings*) ;
    
    * un nœud sans enfant est situé en bas de l'arbre : c'est une **feuille** (*leaf*) ;
    
    * un nœud qui n'est pas une feuille est un **nœud interieur** ;
    
    * des nœuds de même profondeur sont des **voisins** (*neighbors*).


## Implantation

Il existe de nombreuses façons de représenter les arbres.

L'une des méthode classique est de les définir de façon **récursive** comme des nœuds pointant vers d'autres nœuds.

La structure de base est alors le nœud. Celui-ci contient :

* une *valeur* ;
* une liste de pointeurs vers ses *enfants*.

L'arbre enraciné est alors associé à sa racine.

{{ IDE('arbre_classe.py')}}

Il est aussi possible de se passer d'une classe en utilisant des listes au format `[valeur, liste_des_enfants]` :

{{ IDE('arbre_listes.py')}}

## Algorithmes

Dans toute cette partie, on utilise l'implantation sous forme de listes `[valeur, liste_des_enfants]`.

### Taille de l'arbre

On rappelle que la taille d'un arbre est égale au nombre de nœuds qu'il contient. Il est possible de déterminer la taille à l'aide d'une fonction récursive :

```
Fonction taille(arbre)
    Si arbre est une feuille :
        Retourner 1

    t = 1
    Pour chaque sous-arbre de l'arbre:
        t = t + taille(sous-arbre)
    Renvoyer t
```

{{ IDE('taille.py')}}

### Hauteur de l'arbre

On peut calculer la hauteur d'un arbre en utilisant une fonction récursive.

```
Fonction hauteur(arbre)
    Si arbre est une feuille :
        Retourner 1

    Retourner 1 + hauteur maximale de tous les sous-arbres.
```

{{ IDE('hauteur.py')}}

### Parcours de l'arbre

Parcourir un arbre permet de connaître les valeurs de ses nœuds mais aussi d'appliquer un traitement à chacun d'eux.

On retrouve, comme pour les [parcours de graphes](../4_graphes/index.md), deux types de parcours

#### Parcours en largeur

???+ info "Parcours en largeur"

    On visite tous les nœuds d'une certaine profondeur avant de passer à la profondeur suivante ;
    
    ![Parcours en largeur](largeur.png){.center .autolight width=50%}

On peut implémenter un parcours en largeur à l'aide d'une *file*. Un arbre étant par définition *acyclique*, il est inutile de garder trace des sommets déjà visités.

```
Fonction largeur(arbre) :
    file est une File vide
    Enfiler arbre dans file
    Tant que file est non vide :
        Défiler un arbre
        Traiter cet arbre
        Pour chaque sous-arbre de cet arbre :
            Enfiler cet arbre dans file
```

{{ IDE('largeur.py')}}

#### Parcours en profondeur

???+ info "Parcours en profondeur"

    On visite un nœud et tous les descendants avant de passer à ses voisins.

    ![Parcours en profondeur](profondeur.png){.center .autolight width=50%}

    Selon que l'on traite un nœud avant ou après d'avoir traité ses enfants, on parlera de parcours *préfixe* ou *suffixe*.

    ![Deux parcours en profondeur](profondeur_types.png){.center .autolight width=30%}


Les parcours en profondeur peuvent eux être définis de façon récursive. 

```
Fonction préfixe(arbre) :
    Traiter cet arbre  # AVANT les enfants
    Pour chaque sous-arbre de cet arbre :
        Exécuter préfixe(sous-arbre)

Fonction suffixe(arbre) :
    Pour chaque sous-arbre de cet arbre :
        Exécuter suffixe(sous-arbre)
    Traiter cet arbre  # APRES les enfants
```

{{ IDE('profondeur.py')}}