# --- PYODIDE:code --- #
def est_cyclique(graphe):
    for depart in graphe:
        deja_vu = {sommet: False for sommet in graphe}
        pile = []
        pile.append((depart, None))
        deja_vu[depart] = True
        while pile != []:
            sommet, precedent = pile.pop()
            for voisin in graphe[sommet]:
                if voisin != precedent:
                    if deja_vu[voisin]:
                        return True
                    else:
                        deja_vu[voisin] = True
                        pile.append((voisin, sommet))
    return False


graphe_cyclique = {
    "A": ["B", "C", "D"],
    "B": ["A", "D", "E"],
    "C": ["A", "D"],
    "D": ["A", "B", "C", "E"],
    "E": ["B", "D"],
}

assert est_cyclique(graphe_cyclique) is True

graphe_acyclique = {
    "A": ["B", "C", "D"],
    "B": ["A", "E", "F"],
    "C": ["A", "G"],
    "D": ["A"],
    "E": ["B"],
    "F": ["B"],
    "G": ["C", "H"],
    "H": ["G"],
}

assert est_cyclique(graphe_acyclique) is False