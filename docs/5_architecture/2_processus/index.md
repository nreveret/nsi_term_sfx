---
title: Processus et ressources
---

Aussi rapides et efficaces soient-ils nos ordinateurs ne possèdent que peu d'unités de calculs. Pendant des dizaines d'années, ils ne possédaient à vrai dire qu'un seul processeur et ne pouvaient donc accomplir qu'une seule opération à la fois. L'arrivée récente des ordinateurs *multi-cœurs* n'a pas changé fondamentalement les choses : nos ordinateurs ne peuvent pas accomplir au même instant **toutes** les tâches que nous leur demandons.

De façon symétrique, les ressources d'un ordinateur sont limitées : quelques barrettes de mémoire vive, un ou deux disques durs, un moniteur... Il faut bien décider quel programme a accès à la ressource à quel instant : si l'on est en train de travailler sur un traitement de texte tout en écoutant de la musique stockée sur le disque dur, comment gérer le moment où l'on sauvegarde son document ?

Le fonctionnement classique organise les tâches de l'ordinateur en **processus**. Le logiciel chargé de gérer ces processus et de décider lequel s'exécute à quel instant, lequel a accès aux ressources, est le **système d'exploitation**.

## Les processus

???+ info "Processus"

    On peut se représenter un processus comme une tâche, comme un programme s'**exécutant** sur l'ordinateur.

    Tant que l'on n'affiche aucune image à l'écran, le programme d'affichage n'est qu'un programme, une suite de $0$ et de $1$ dans la mémoire. A l'instant ou l'on double-clique sur une image (ou que l'on l'appelle à partir d'un terminal), ce programme est chargé dans la mémoire-vive et s'exécute : il devient un processus.

Cette vision mérite toutefois d'être précisée : 

* d'une part, il faut comprendre que tout est processus. Le système d'exploitation qui gère les processus, s'exécute lui-même sous forme de processus

* d'autre part, un programme qui s'exécute peut correspondre à plusieurs processus. Le logiciel utilisé pour taper ce document utilise actuellement $8$ processus différents !

Lors du démarrage, l'ordinateur charge un premier processus. Sur les systèmes *Linux*, il s'agit de *init*. C'est ce processus qui va démarrer le système d'exploitation et lancer les suivants. 

???+ "`PID`"

    Chaque processus est associé à différentes ressources : des données en mémoire, le numéro de la prochaine instruction à exécuter... Il est identifié par un numéro unique, son *Process ID* ou `PID`.

A part ce premier processus, tous les autres sont créés par d'autres processus : chacun est l'**enfant** d'un processus **parent**. Cette structure permet de construire un *arbre* présentant l'ensemble des processus s'exécutant sur l'ordinateur.

???+ "`PPID`"

    Un processus garde trace du processus qui l'a créé, de son *parent*.
    
    Cette information est nommée *Parent Process ID* ou `PPID`.

Les processus actifs sur une machine peuvent donc être organisés hiérarchiquement : on peut construire un *arbre des processus*.

![L'arbre des processus](process_tree_.png){width=60% .center .autolight}


???+ info "Visualiser les processus"

    Il est facile de lister les processus s'exécutant sur une machine :

    * sur une machine *Linux*, on utilise la commande `ps` dans un terminal ;
    * sous *Windows*, on tape `Get-Process` dans le powershell.

    <center>
    ![Les processus sous *Ubuntu*](ps_.png){width=40% .autolight}
    ![Les processus sous *Windows*](get_process_.png){width=41.5% .autolight}
    </center>
    
    La commande `pstree` de *Linux* permet quant à elle de visualiser l'arbre des processus.


## Organisation temporelle

On l'a dit, tous les processus ne s'exécutent pas en même temps.

???+ info "États des processus"
 
    À chaque instant, tous les processus se trouvent dans l'un des trois états suivants :

    * **prêt** : le processus est prêt à être exécuté. C'est le système d'exploitation qui lui rendra la main pour effectuer un ou plusieurs calculs ;
    
    * **élu** : le processus est en cours d'exécution sur le processeur ;
    
    * **bloqué** : le processus attend qu'une ressource (un accès à la mémoire par exemple) ou qu'un évènement (saisi au clavier...) soit effectué.

    ![Cycle de vie d'un processus](etats.svg){width=50% .center .autolight}
Lors de son exécution, le processus va consommer un certain nombres de cycles de processeurs : **il n'est pas du tout certain qu'il s'exécutera en une seule passe**. Les processus passent donc d'un état à l'autre lors de leur exécution. 

Afin de rentabiliser la puissance de calcul du processeur, le processus exécuté peut alors changer d'un instant à l'autre si par exemple :

* le processus en cours passe en attente d'une ressource ;

* un processus en attente obtient la ressource souhaitée et est de nouveau prêt à être exécuté ;

* le processus en cours se termine.

Le choix du processus auquel donner la main est fait par le système d'exploitation. Le choix peut s'appuyer sur les critères suivants :

* le taux d'utilisation du processeur (que l'on cherche alors à maximiser) ;

* le nombre de processus terminés par seconde (à maximiser) ;

* le temps d'exécution total d'un processus, temps de calcul et d'attente compris (à minimiser) ;

* le temps d'attente (à minimiser).

Dès lors les algorithmes peuvent utiliser les méthodes suivantes :


* *Premier Arrivé, Premier Servi* : les processus sont classés dans une file. A chaque changement, le système d'exploitation donne la main au processus en tête de file ;

* *Le plus court* : on donne la main au processus qui demande le moins de temps de calcul ;

* *Le prioritaire* : les processus n'ont pas tous la même importance. La gestion d'un évènement de l'utilisateur (une frappe au clavier par exemple) doit être rapide alors que d'autres processus peuvent sans problème s'exécuter en une seconde. Chaque processus possède donc une note de priorité. Dans cette méthode, on exécute en premier les processus les plus prioritaires.

La liste ci-dessus n'est pas exhaustive, d'autres méthodes existent. Il est aussi possible de mélanger les méthodes (par exemple créer différentes files en fonction des degrés de priorités et appliquer un algorithme différent à chaque file...).

## Accès aux ressources

L'accès aux ressources est aussi un point à prendre en compte. Considérons la situation suivante : 

* Un processus `P1` demande l'accès à une ressource `R1` (il souhaite par exemple lire une donnée en mémoire) ;
* Un processus `P2` doit lire une donnée sur la mémoire `R2`.

Afin que l'exécution d'un processus se fasse de façon sécurisée, lorsqu'il accède à une ressource, celle-ci lui est allouée et indisponible pour les autre processus. Dans le cas présent, `P1` et `P2` ne demandent pas les mêmes ressources, ils peuvent être exécutés dans un ordre ou l'autre sans problème.

![Fonctionnement possible](normal.svg){width=30% .center .autolight}

La situation devient problématique dans le cas suivant :

* Le processus `P1` a accès à la ressource `R1`. Celle-ci est indisponible pour les autres processus
* Le processus `P2` a accès à la ressource `R2`. Elle est aussi bloquée
* Le processus `P1` demande l'accès à `R2` : il passe en situation d'attente et le système d'exploitation donne la main à `P2`
* `P2` demande l'accès à `R1` qui est bloquée par `P1` qui est en attente de `R2` elle-même bloquée par `P2`... !

![Interblocage](interblocage.svg){width=40% .center .autolight}

Cette situation s'appelle l'**interblocage** (*deadlock* en anglais).

Une méthode pour éviter cette situation est de connaître à l'avance les ressources qui seront utilisées par les processus. On donnera la main à des processus *sûrs*, n'engendra pas d'interblocages. Il est aussi possible d'allouer les ressources en tenant compte du degré de priorité du processus demandeur.

Si la situation se présente tout de même le système d'exploitation peut par exemple terminer un des ou tous les processus impliqués dans l'interblocage ou reprendre la main sur la ressources bloquante et l'attribuer de façon autoritaire à l'un des processus jusqu'à ce que le blocage soit levé.
