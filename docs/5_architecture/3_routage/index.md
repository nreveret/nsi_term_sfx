---
title: Protocoles de routage
---

Internet est grand ! En août 2020, Google référence plus de $55$ milliards de pages web...

Ces pages sont enregistrées sur des millions de serveurs. Lorsqu'un utilisateur souhaite consulter une page, une requête parcourt le *web* jusqu'au serveur et celui-ci renvoie les données demandées.

Bien sûr il n'existe pas un câble unique entre chaque utilisateur d'internet (relier $n$ utilisateurs nécessiteraient alors $\frac{n(n-1)}{2}$ câbles !). Le réseau est construit de façon décentralisée, distribuée. Cette structure est plus efficace (en particulier dans le cas de la panne d'un routeur) mais complique l'acheminement d'un paquet entre deux postes : quelle route choisir lorsque personne ne connaît l'ensemble de la carte du réseau ?

![Les différentes architectures de réseau](distributed_network.png){width=40% .center .autolight}

## Identification des réseaux

Les nœuds d'un réseau informatique sont les *routeurs*. Ceux-ci sont à l'interface entre le réseau principal et des *sous-réseaux*.

![Un sous-réseau](subnet.png){width=60% .center .autolight}

Lorsqu'une machine (un routeur en particulier) doit transmettre un message, il consulte sa *table de routage*.

???+ info "Table de routage"

    La table de routage d'une machine est une liste de relations du type : `{destination: (prochain nœud, distance)}`.
    
    Une table ne peut pas contenir les adresses de tous les réseaux. Elle contient donc aussi une ligne pour rediriger les messages *par défaut*.

    Il est possible de consulter les tables de routages de notre poste en utilisant les commandes suivantes :

    * dans un terminal *Linux* : `netstat -rn` ;

    * dans le *powershell* de *Windows* : `Get-NetRoute`.

    ![Table de routage *Windows*](get-netroute.png){width=70% .center .autolight}

Une adresse IPv4 s'écrit sur $32$ bits (la version 6, en cours de mise en place, utilise $128$ bits). Cette nomenclature permet de manipuler $2^{32} =4\,294\,967\,296$, ce qui est beaucoup trop peu face à la multiplication des terminaux connectés à internet... Il a donc fallu trouver une astuce.

??? note "Adresses publique et privée"
        
    Le routeur, à l'interface entre différents réseaux, possède plusieurs adresses IP.
    
    Par exemple la box d'une maison particulière possède une adresse privée (correspondant au réseau local) et une adresse publique (sur internet).
    
    Autant l'adresse privée peut être identique entre deux sous-réseaux (il y a fort à parier que votre box a la même IP `192.168.1.1` privée que celle de votre voisin), autant les adresses publiques sont *obligatoirement* différentes sans quoi les messages n'arriveraient pas au bon destinataire.

Il est donc courant que, dans une table de routage, les destinations soient écrites avec la notation *Classless Inter-Domain Routing* (*CIDR*). Dans cette notation, on ajoute à l'adresse IP du routeur un `/n` dans lequel `n` est le nombre de bits de l'adresse qui sont communs à toutes les machines du sous-réseau (en partant du bit de poids fort, à gauche).

Ces `n` premiers bits permettent d'identifier le réseau auquel appartient une machine. Les `32 - n` derniers identifient la machine dans ce réseau.

Ainsi, l'adresse `192.168.1.0/24` identifie le réseau par les $24$ premiers bits ce qui en laisse $8$ pour nommer les machines au sein du réseau. Comme $2^8=256$, on peut utiliser cette adresse de réseau pour les structures de petite taille (moins de $256$ postes).

??? info "Deux adresses réservées"

    Dans le faits, deux adresses sont réservées :
    
    * l'adresse dans laquelle tous les bits de la partie machine sont nuls est **l'adresse du réseau** ;
    
    * l'adresse dans laquelle tous les bits de la partie machine valent `1` est **l'adresse de multidiffusion** (ou *broadcast).

    Dans le cas d'un réseau `192.168.1.0/24`, il ne reste donc que $2^8-2=254$ adresses disponibles pour des machines.

La notation CIDR permet aussi de déterminer le *masque de sous-réseau*. Il s'agit de l'adresse dans laquelle tous les bits de la partie réseau ont pris la valeur `1`.

## Le plus court chemin dans un graphe

Un réseau informatique peut être modélisé comme un graphe pondéré :

* les sommets sont les routeurs, serveurs et autres ordinateurs personnels ;

* les arcs sont les câbles en cuivre, en fibre optique, les connexions sans-fil, les câbles sous-marins... ;

* les poids peuvent prendre différentes valeurs : temps de connexion, bande passante de la liaison...

Le poids total d'un chemin dans un graphe est égal à la somme des poids de toutes les arêtes empruntées.

On pourrait encore affiner la description en considérant que le graphe est orienté (la liaison $A \rightarrow B$ diffère de $B \rightarrow A$) mais ce n'est pas nécessaire dans le cadre de ce cours.

Considérons le graphe suivant.

<center>

```mermaid
graph LR
    E(E)
    C(C)
    A(A)
    B(B)
    D(D)
    F(F)
    
    A ---|1| C
    A ---|2| B
    E ---|2| F
    A ---|5| D
    D ---|1| E
    B ---|2| C
    B ---|3| D
    C ---|1| E
    C ---|3| D
    D ---|5| F
```
</center>


On souhaite déterminer le plus court chemin entre les sommets A et F.

Une première approche est de lister tous les chemins existant entre A et F et de mesurer leur poids respectifs. On retient le chemin de poids minimal.

On obtient le chemin A → C → E → F de poids total 5.

L'algorithme classique est plus subtil. Il s'agit de l'algorithme de [Dijkstra](https://fr.wikipedia.org/wiki/Algorithme_de_Dijkstra). L'idée est de parcourir tout le graphe à partir du sommet de départ en faisant en sorte que chaque sommet soit associé à son prédécesseur dans le plus court chemin le reliant à l'origine.

## *Routing Information Protocol*

Le protocole *Routing information Protocol* qui s'appuie lui-même sur l'algorithme de [Bellman-Ford](https://fr.wikipedia.org/wiki/Algorithme_de_Bellman-Ford). Ces algorithmes utilisent une technique à *vecteur de distance*.

!!! info "Protocole RIP"

    Dans le protocole RIP, chaque routeur possède une table de routage comprenant trois informations :
    
    * la destination à joindre,
    
    * le prochain routeur à qui transmettre le message,
    
    * le poids total du chemin.

    Les poids sont égaux au nombre de routeurs traversés. Les voisins d'un routeur sont donc à une distance de 1.

    À intervalles réguliers (initialement toutes les $30$ secondes) un routeur envoie sa table **à tous ses voisins**.
    
    Le routeur met alors à jour sa table de routage :
    
    * si une table reçue contient des informations concernant une destination inconnue, celle-ci est ajoutée à la table. Le prochain routeur utilisé est celui ayant transmis la table ,la distance celle transmise augmentée de 1 (nombre de saut pour joindre ce voisin) ;
    
    * si une table reçue contient des informations concernant une destination déjà connue mais avec une distance supérieure : cette route est ignorée ;

    * si par contre cette nouvelle route est plus courte (même en ajoutant 1 saut pour joindre le voisin), la table est modifiée.


Outre sa simplicité, ce protocole a l'avantage de converger (de tendre) vers une solution optimale. Il réagit très rapidement aux  bonnes nouvelles » (l'existence d'un nouveau chemin très court se propage très rapidement le long du réseau).

Par contre, si un routeur tombe en panne, cette information met beaucoup de temps à être diffusée et peut même, à long terme, engendrer des chemins de taille infinie dans les tables ! Pour cette raison, l'algorithme *RIP* est limité au chemins de taille inférieures à $16$ sauts ce qui restreint son utilisation à certains "petits" réseaux.

## *Open Shortest Path First*

Le protocole *Open Shortest Path First* (*OSPF*) date des années 1990 et a été développé afin de pallier aux faiblesses des *RIP* en particulier sa faible adaptation aux réseaux des grande taille.

Il fait partie des protocoles par *état de lien*.


!!! info "Protocole OSPF"

    Dans le protocole OSPF, chaque routeur possède la carte totale du réseau. A chaque route est associé un poids.
    
    Ce poids se calcule communément en utilisant la formule :
    
    $$poids = \frac{débit}{10^8}$$
    
    dans laquelle $débit$ est la bande passante maximale de la liaison en $bits/s$.

    Connaissant toutes ses informations, le routeur peut alors déterminer le plus court chemin vers la destination souhaitée.
    
    Il peut par exemple utiliser l'algorithme de Dijkstra.


Chaque routeur contient donc l'ensemble de la carte du réseau... A l'échelle d'internet c'est impossible. C'est pourquoi *OSPF* fonctionne à l'échelle de **systèmes autonomes**. Un système autonome peut par exemple être le réseau géré par une université ou l'ensemble des routeurs administrés par un fournisseur d'accès. Il en existe près de $100\,000$ à l'échelle du globe !

![Système autonome](ospf.jpg){width=50% .center .autolight}

Un système autonome est construit autour d'une *épine dorsale* (*backbone* en anglais). Celle-ci permet de mettre en relation différentes *zones* (*area* en anglais). On peut alors distinguer différents types de routeurs selon qu'ils sont intérieurs (de l'épine dorsale ou d'une zone) ou à l'interface entre elles.

Ces routeurs de frontières ont un statut particulier car ils doivent connaître l'ensemble des informations des deux espaces auxquels ils appartiennent (épine dorsale et zone). Les routeurs internes eux ne connaissent que les informations concernant leur espace propre.

Lorsqu'un message est adressé à l'intérieur d'une zone, le routage est direct. Si le destinataire est par contre situé dans une autre zone, le routage se fait en trois temps :

1. routage jusqu'au routeur de frontière le plus proche
2. routage dans l'épine dorsale jusqu'à la zone de destination
3. routage dans la zone de destination

Et si le message est destiné à un poste situé hors du système autonome ? Il est alors acheminé à un routeur de frontière d'épine dorsale et transmis à un autre protocole qui aura pour tâche de l'acheminer au bon système autonome. Le plus utilisé actuellement est *Border Gateway Protocol* (*BGP*) qui recense plus de $500\,000$ routes reliant des systèmes autonomes (notons au passage que ce nombre est si important que d'anciens routeurs n'ont pas la mémoire suffisante pour sauvegarder la table de routage et deviennent dès lors dysfonctionnels).
