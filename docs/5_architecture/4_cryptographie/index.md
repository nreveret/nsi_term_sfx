---
title: Cryptographie
---

De tout temps, les hommes ont eu des secrets. Partager un secret est compliqué : on souhaite le dire à un proche mais sans que les autres ne l'entendent...

Pendant longtemps, le partage d'information a reposé sur des contraintes physiques : si personne ne m'entend dire le secret il n'y a pas de problème. Plus tard on a inventé les première méthodes de cryptage (du grec *kryptos*, caché). On parle aussi de méthodes de *chiffrement*. Pas sûr que César ait réellement utilisé le *chiffre de César*... Les techniques évoluant, certaines méthodes "simples" se sont avérées efficaces pendant longtemps. Le chiffre de [Vigenère](https://fr.wikipedia.org/wiki/Chiffre_de_Vigen%C3%A8re) a ainsi tenu pendant trois siècles !

L'arrivée récente des ordinateurs a bousculé la donne : il est très facile désormais de crypter des messages mais il est aussi très simple de lancer une attaque *force brute* sur un message en testant toutes les clés possibles pour peu qu'elles soient de taille raisonnable...

Pourtant les besoins de sécurité des données ont augmenté : outre le caractère sensible de certaines informations (coordonnées bancaires...), la numérisation de la société a exposé une grande part de la vie personnelle de chacun. Aujourd'hui le moindre *smartphone* permet de stocker les photos personnelles du propriétaire, d'enregistrer les pages web qu'il visite voir même de suivre ses données médicales. Et tout cela est stocké en ligne dans le *cloud*. Dès lors il est indispensable que ces informations soient bien chiffrées.

## Chiffrement symétrique

Alice veut faire passer une information à Bob. Que cette information soit du texte, une image, un son, il est possible de la numériser et donc de l'écrire sous forme binaire. Par exemple le message `Salut Bob` s'écrit en binaire `01010011 01100001 01101100 01110101 01110100 00100000 01000010 01101111 01100010` grâce au code ASCII.

???+ info "Chiffrement symétrique"

    Les méthodes de cryptographie les plus simples sont dites **symétriques** : la *clé* utilisée pour chiffrer le message est la même que celle utilisée pour le déchiffrer.

    ![Chiffrement symétrique](symetrique.png){width=75% .center .autolight}

On connaît le chiffre de César, ou « `#!py "A"` vaut `#!py "K"`" : on décale chaque lettre d'un certain rang dans l'alphabet. La longueur du décalage est la clé. Si jamais on sort de l'alphabet, il suffit de boucler (la lettre qui suit `#!py "Z"` devient `#!py "A"`). Pour déchiffrer, on utilise la même clé mais on décale dans l'autre sens.

Il s'agit d'un chiffre par *substitution mono-alphabétique* : on se contente de remplacer une lettre par une autre. La version moderne travaille sur les bits. Partant par exemple d'un octet, on peut mélanger l'ordre des bits.

On aura par exemple la transformation suivante en échangeant les 4 bits de poids forts avec ceux de poids faibles:

<center>
`#!py "c"` → binaire avant échange `#!py 01100011`  → binaire après échange `#!py 00110110` → `#!py "c"`
</center>

??? example "La fonction `echange`"

    On utilise les propriétés de la représentation en binaire :
    
    * diviser un nombre par `#!py 2` supprime son bit de poids faible : `#!py 7 // 2 = 3` ;
    
    * diviser un nombre par `#!py 2**4` supprime les 4 bits de poids faible ;

    * calculer le reste d'un nombre par `#!py 2**4` ne renvoie que les bits de poids faibles.

    {{ IDE('echange') }}

??? example "Chiffrement par `echange`"

    On rappelle que :
    
    * `#!py ord` renvoie le code Unicode d'un caractère ;
    
    * `#!py chr` renvoie le caractère associé à un code Unicode.

    {{ IDE('codage_echange') }}

Une autre solution classique est d'utiliser l'opérateur booléen $XOR$ (ou exclusif) dont on rappelle la table ci-dessous :

<center>

|  $A$  |  $B$  | $XOR$ |
| :---: | :---: | :---: |
|  $0$  |  $0$  |  $0$  |
|  $0$  |  $1$  |  $1$  |
|  $1$  |  $0$  |  $1$  |
|  $1$  |  $1$  |  $0$  |

</center>

Si l'on choisit pour clé un entier s'écrivant sur un octet, on peut appliquer cet opérateur à chaque octet du message, bit par bit.

??? example "Chiffrement par `XOR`"

    L'opérateur $XOR$ est noté `#!py ^` en Python.
    
    {{ IDE('codage_xor') }}
    
Ces deux méthodes sont faciles à comprendre et à casser mais elles restent efficaces lorsqu'on les combine entre elles. Ainsi l'algorithme [*Advance Encryption Standart*](https://proprivacy.com/guides/aes-encryption) utilisé régulièrement de nos jours avec une clé de $128$ bits et considéré comme sûr, utilise entre autres méthodes, des décalages de bits et des `XOR`. Aussi compliqué soit-il, il reste toutefois un algorithme de chiffrement symétrique.

## Chiffrement asymétrique

Les méthodes de chiffrement symétrique, aussi subtiles et techniques soient-elles poseront toujours un problème : comment Alice peut-elle transmettre de façon sécurisée la clé à Bob ? Pour être sûre d'elle il faudrait crypter la clé. Mais dans ce cas il faut aussi transmettre la clé de cryptage permettant de crypter la clé... On ne s'en sort pas !

Une solution est apparue au milieu des années 1970 : le chiffrement **asymétrique**.

???+ info "Chiffrement asymétrique"

    Un chiffrement **asymétrique** utilise deux clés : une clé publique et une autre privée.
    
    La clé publique peut être transmise en clair à tous les utilisateurs. Elle permet de coder le message.
    
    La clé privée est gardée secrète. Elle permet de décoder lz message.
    
    Intercepter la clé publique ne présente aucun intérêt : elle ne sert qu'à chiffrer !
    
    ![Chiffrement asymétrique](asymetrique.png){width=70% .center .autolight}

Lorsque deux utilisateurs communiquent en utilisant un chiffrement asymétrique, chacun a généré un couple de clés publique et privée :

1. Alice chiffre le message en utilisant la clé publique de Bob ;
2. Bob déchiffre le message en utilisant sa clé privée ;
3. Bob répond en chiffrant le message à l'aide de la clé publique d'Alice ;
4. Alice déchiffre le message en utilisant sa clé privée.


La méthode la plus commune de chiffrement asymétrique utilisée aujourd'hui est **RSA** développée en 1977 par Ron Rivest, Adi Shamir, et Leonard Adleman. Basée sur des propriétés mathématiques assez abordables (niveau bac+2), sa sécurité repose sur la difficulté actuelle des ordinateurs à décomposer des nombres entiers en facteurs premiers.

En effet, il est facile de calculer que $1\,299\,709\times1\,159\,523=1\,507\,042\,478\,807$ ($1\,299\,709$ et $1\,159\,523$ sont des nombres premiers). Il est par contre beaucoup plus compliqué (à la main comme à l'aide d'un ordinateur) de décomposer $1\,507\,042\,478\,807$ sous la forme  $1\,299\,709\times1\,159\,523$.

![Fonctionnement de RSE](rsa.png){width=50% .center .autolight}

Ces méthodes de chiffrement asymétrique sont néanmoins assez lourdes à mettre en place en termes de calculs aussi les utilise-t-on surtout pour transmettre de façon sécurisée des clés associées à des chiffrements symétriques. Ceux-ci sont en effet moins gourmands en calculs. Une fois la clé transmise, on quitte les méthodes asymétriques.

## Signature digitale

Les méthodes de chiffrement asymétriques permettent aussi de **signer** des messages, c'est à dire de garantir à un utilisateur que son interlocuteur est bien qui il prétend être.

Le principe repose sur la difficulté, connaissant une clé publique, de calculer la valeur de la clé privée.

???+ info "Signature digitale"

    Supposons qu'Alice souhaite signer un message $m$ afin de le transmettre à Bob :

    1. elle va tout d'abord générer un couple de clé publique et privée ;
    2. elle chiffre le message $m$ à l'aide de sa clé privée afin de calculer $m_c$;
    3. elle transmet à Bob le message $m$ ainsi que sa version chiffrée et la clé publique ;
    4. Bob déchiffre alors $m_c$ en utilisant la clé publique. Il obtient le message déchiffré $m_d$ ;
    5. Si $m_d=m$ alors Bob est « certain » que son interlocuteur est bien Alice.

    ![Signature](signature.png){width=50% .center .autolight}

## Protocole HTTPS

Lorsque l'on visite une page sur le web, le trafic passe par de nombreux routeurs et peut alors être intercepté par des pirates. Ceux-ci peuvent par exemple filtrer les données échangées entre le client et le serveur afin de les lire ou d'en injecter de nouvelles : c'est une attaque *Man In The Middle*.

![Man In The Middle](man_middle.jpg){width=40% .center .autolight}

L'une des façons de se protéger de ce genre d'attaque est d'utiliser le protocole HTTPS.

???+ info "Protocole HTTPS"

    L'idée du protocole HTTPS est la suivante :

    1. le client contacte un serveur et demande une connexion sécurisée ;

    2. le serveur répond en confirmant pouvoir dialoguer de manière sécurisée et en produisant un certificat garantissant qu'il est bien le serveur en question et pas un serveur pirate déguisé ;

    3. ces certificats électroniques sont délivrés par des autorités tierces en laquelle tout le monde a confiance. Le client peut ainsi vérifier de son côté auprès d'un de ces sites de référence que le certificat est valide (et donc qu'il parle au bon serveur) ;

    4. le certificat contient aussi une clé publique. Le client utilise celle-ci afin de chiffrer une autre clé (associé à un chiffrement symétrique) qu'il transmet au serveur ;

    5. le serveur déchiffre cette dernière grâce à sa clé privée et dès lors, client et serveur peuvent échanger de façon sécurisée grâce à un chiffrement symétrique.

    ![Le protocole HTTPs](ssl.png){width=70% .center .autolight}