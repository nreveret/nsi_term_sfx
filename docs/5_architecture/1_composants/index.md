---
title: Composants intégrés sur une puce
---

Un ordinateur regroupe de nombreux éléments : outre le processeur et la mémoire, il faut intégrer un écran, des périphériques, une carte réseau...

![Architecture classique](computer_architecture.jpg){width=50% .center .autolight}

L'évolution moderne des ordinateurs et leur miniaturisation ont menés à deux tendance actuelles :

* la dissipation thermique (le fait que les composants électroniques chauffent) a contraint les fabricants à stopper la miniaturisation. Il n'est aujourd'hui plus vraiment possible de placer plus de transistors sur un processeur car l'ensemble chaufferait trop. L'une des solutions est de fabriquer des processeurs multi-cœurs : au lieu d'intégrer plus de transistors sur un processeurs, on met plus de processeurs dans un ordinateurs

* le développement des appareils mobiles (*smartphones*, tablettes) a conduit les fabricants à réduire l'encombrement des composants électroniques

Cette seconde tendance a mené à la création et l'utilisation de circuits sur puce, *System On Chip* en anglais (*SoC*).

## Présentation générale

Observons une carte mère.

![Gigabyte MW-51 HP0](Gigabyte_MW51_HP0.jpg){width=40% .center .autolight}

![Explications](gigabyte_mw51-hp0_diagram.png){width=50% .center .autolight}

On retrouve les éléments classiques de l'architecture de *Von Neumann*.

??? info "Rappel : architecture de *Von Neumann*"

    ![Architecture de Von Neumann](von_neumann.svg){width=50% .center .autolight}

    On retrouve :
    
    * l'unité arithmétique et logique (UAL ou ALU en anglais) ou unité de traitement qui effectue les opérations de base ;
    * l'unité de contrôle qui garde trace de l'opération en cours et détermine la prochaine ;
    * la mémoire qui contient à la fois les données et le programme ;
    * les dispositifs d'entrée-sortie, qui permettent de communiquer avec le monde extérieur.

Comme on peut le voir, cette carte laisse de la place pour de nombreux composants que l'on vient ajouter, brancher dessus.

Cette façon de procéder ne peut pas s'accorder avec l'utilisation actuelle des appareils mobiles : il est très compliqué de changer un composant électronique sur un *smartphone* du simple fait de leur taille... En réalité, les composants sont d'ores et déjà intégrés sur une carte en silicium, équivalente à la carte mère mais intégrant dès sa conception l'ensemble des composants.

![Le *SoC* Snapgragon 835 de Qualcom](qualcomm-snapdragon-835.jpg){width=30% .center .autolight}

![Explications](Snapdragon_835-SoC_Overview.png){width=60% .center .autolight}

Ces *SoC* regroupent, **sur une seule puce**, l'ensemble des composants de l'ordinateur classique (processeur(s), mémoires, carte(s) réseau(x), liens vers les périphériques, ...).

## Avantages et inconvénients

Ces systèmes ont bien entendu été rendus possibles grâce à la miniaturisation des composants. Mais, comme expliqué plus haut, celle-ci s'accompagne d'importantes déperditions énergétiques... Pour y pallier, les fabricants ont fait le choix d'intégrer des pièces de moins bonnes factures dans leur circuits. Ainsi les processeurs de nos téléphones ne peuvent pas rivaliser avec ceux de nos ordinateurs fixes.

Ce choix présente toutefois un avantage majeur : inutile d'intégrer de gros systèmes de refroidissement. La taille des systèmes s'en voit encore réduite et leur consommation électrique aussi.

La miniaturisation permet aussi un gain de temps : les composants étant plus proches, le temps de transport de l'information s'en voit diminué.

Les faibles performances des composants intégrés sont toutefois compensées par une meilleure intégration. En effet, dans la mesure où toutes les cartes sont construites sur le même modèle, avec exactement les mêmes composants (à la différence des cartes mères classiques qui peuvent accueillir de nombreux composants différents), leur architecture, leur jeu d'instruction  est conçu spécifiquement et parfaitement adapté. Il est ainsi plus efficace.

Ces systèmes présentent aussi l'inconvénient de ne pas être réparables : si un des composant tombe en panne, c'est l'ensemble du système qu'il faut remplacer...,

Enfin, ils sont compliqués à concevoir et donc chers à fabriquer et à acheter.