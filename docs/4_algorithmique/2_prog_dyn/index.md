---
title: Programmation dynamique
---

Bien souvent, le nombre de calculs nécessaires à la résolution d'un problème augmente rapidement avec la taille du problème (comme pour le [voyageur de commerce](https://fr.wikipedia.org/wiki/Probl%C3%A8me_du_voyageur_de_commerce)). Dans certains cas toutefois, il est possible de résoudre un grand problème en résolvant plusieurs problèmes de taille inférieure. C'est l'approche que l'on peut retrouver dans la méthode « *Diviser pour régner* ».

Il arrive parfois que cette approche multiplie les calculs redondants. C'est le cas lorsque les sous-problèmes de répètent. On dit qu'ils sont *inter-dépendants*. On utilise alors la *programmation dynamique*.

???+ info "Programmation dynamique"

    La **programmation dynamique** est un méthode de résolution de problème visant à **découper le problème initial en sous-problèmes**.
    
    Ces sous-problèmes sont toutefois **inter-dépendants**.
    
    Dans ce cas, on garde trace des solutions aux sous-problèmes afin de ne pas les calculer à nouveau. C'est la **mémoïsation**.
    
    On peut alors résoudre le problème :
    
    * de façon *récursive* ou **descendante** (**top-down**) ;
    
    * de façon *itérative* ou **ascendante** (**bottom-up**).
  
    ![*Top-Down* vs *Bottom-Up*](top_down_bottom_up.png){width=40% .center .autolight}


## La suite de *Fibonacci*

Au XIII-ième siècle, Léonard de Pise dit Fibonacci pose le problème récréatif suivant :

* on se donne un couple de lapins juvéniles ;

* après deux mois, ce couple engendre un nouveau couple de lapins ;

* tous les couples se reproduisent à partir du troisième mois suivant leur naissance et engendre alors un couple par mois ;

* combien de couples y-aura-t'il après $n$ mois ?

![Les lapins de Fibonacci](lapins.png){width=40% .center .autolight}

Cette énigme amène à considérer la suite $(F_n)$ suivante :

$$F_0 = 0$$

$$F_1 = 1$$

$$\forall n >= 2,\,F_n=F_{n-1}+F_{n-2}$$

Une application immédiate de cette formule de récurrence donne la fonction ci-dessous.

??? example "Approche naïve"

    Cette approche est valide pour de petites valeurs de $n$. Lancer le calcul de `fibonacci(50)` fera par contre planter le navigateur...

    {{ IDE('fibo_simple.py')}}
    
Le code précédent est inefficace pour de grandes valeurs de $n$ du fait de l'inter-dépendance des sous-problèmes.

![Problèmes inter-dépendants](fibo.svg){.autolight .center width=50%}

On propose ci-dessous deux solutions.

??? example "Solution descendante"

    Dans cette solution récursive, on garde trace des valeurs déjà calculées dans un dictionnaire `fibo` défini dans le corps du programme afin d'être accessible et modifiable quel que soit le niveau de profondeur de la récursivité.
    
    {{ IDE('fibo_rec.py')}}
    
??? example "Solution ascendante"

    Dans cette solution itérative, on complète un tableau `fibo` à partir de l'indice `0` en progressant jusqu'à l'indice `n`.
    
    {{ IDE('fibo_iter.py')}}
    
??? note "Solution efficace (hors-programme)"

    Certains résultats mathématiques permettent d'optimiser ce code. On quitte alors la programmation dynamique pour aller vers l'informatique mathématique.
    
    Dans la méthode ci-dessous on utilise la relation matricielle ci-dessous :
    
    $$\begin{pmatrix}F_{n+1}&F_{n}\\F_{n}&F_{n-1}\end{pmatrix}=\begin{pmatrix}1&1\\1&0\end{pmatrix}^{n}$$
    
    {{ IDE('fibo_efficace.py')}}
    
## Chemin maximal dans une grille


On considère désormais des grilles non vides contenant des entiers positifs ou nuls telles que celle représentée ci-dessous :

$$
\begin{array}{|c|c|c|c|}
\hline
3&5&6\\
\hline
4&9&1\\
\hline
\end{array}
$$

On cherche à déterminer la somme maximale obtenue en parcourant la grille :

*  depuis la case **en haut à gauche** (case d'origine)
*  jusqu'à la case **en bas à droite** (case finale)
*  en ne faisant que des déplacements vers la droite :arrow_right: ou vers le bas :arrow_down:.

Dans l'exemple ci-dessus, la somme maximale vaut $18$. Elle est obtenue en faisant le parcours $3$ :arrow_right: $5$  :arrow_down: $9$ :arrow_right: $1$.

On cherche donc, étant donnée une grille de dimensions $n \times m$ ($n$ lignes et $m$ colonnes), à calculer la somme maximale possible d'un chemin se terminant sur la case $(n-1,m-1)$. On note $somme(n-1,m-1)$ cette valeur.

Ce problème se prête bien à la programmation dynamique. En effet, pour atteindre la case de coordonnées $(i,j)$ :

* si cette case est la case d'origine, le chemin maximal a pour somme la valeur d'origine ;
* si cette case est sur la première ligne, on ne peut venir que de la case de gauche $(i, j - 1)$ ;
* si cette case est sur la première colonne, on ne peut venir que de la case du dessus $(i - 1, j)$ ;
* dans les autres cas, on vient soit la case de gauche $(i,j-1)$ soit de celle du dessus $(i-1,j)$. On choisit la case amenant la plus grande somme.

On a donc notre formule de récurrence :

$$
somme(i, j) = \begin{cases}
  grille(i, j)&\text{   si }(i,j)=(0,0)\\
  grille(i, j) + somme(i, j-1)&\text{   si }i=0\\
  grille(i, j) + somme(i-1, j)&\text{   si }j=0\\
  grille(i, j) + \max\left(somme(i, j-1)~,~somme(i-1, j)\right)&\text{ partout ailleurs}\\
\end{cases}
$$

Attention toutefois : les chemins sont inter-dépendants. Une approche récursive naïve repassera plusieurs fois par la même case.

On va donc garder trace des valeurs des sommes maximales dans un tableau `maxis`. On **mémoïse**.

??? example "Version récursive"

    Dans cette version on utilise une fonction annexe `aux` qui prend en paramètres les coordonnées `i` et `j` d'une case, calcule et renvoie la valeur de `maxis[i][j]` (c'est à dire la somme maximale que l'on peut obtenir en allant de l'origine à la case `(i, j)`).
    
    Le tableau `maxis` est initialisé dans la fonction principale `somme_maximale`. On le complète initialement avec la valeur `#!py None` afin de facilement distinguer les cellules déjà calculées de celles restant à calculer.
    
    Cette version remonte les chemins depuis la case finale (en bas à droite) jusqu'à la case d'origine. Chaque pas « en arrière » correspond à un appel récursif. 

    {{ IDE('somme_rec.py')}}
    
??? example "Version itérative"

    On complète le tableau `maxis` mais cette fois-ci on part de l'origine de la grille et on progresse, colonne par colonne et ligne par ligne jusqu'à atteindre la case finale.

    Le tableau `maxis` est initialisé aux bonnes dimensions en ne contenant que des valeurs nulles.

    La première ligne et la première colonne de cette liste sont traitées à part.

    {{ IDE('somme_iter.py')}}

??? example "Version efficace"

    Là encore, il s'agit d'une version partant de la case d'origine et progressant jusqu'à la case finale.
    
    On remarque toutefois ici que le calcul de `maxis[i][j]` ne fait intervenir que les valeurs du dessus et de gauche.
    
    Il n'est donc pas nécessaire de stocker en mémoire l'ensemble du tableau `maxis`. On peut travailler avec une simple ligne `ligne_maxis` aussi large que la grille.
    Cette liste prend initialement comme valeurs les sommes maximales de chemins reliant l'origine à chacune des cases de la première ligne de la grille.
    
    On met ensuite à jour cette liste ligne par ligne jusqu'à ce qu'elle contienne les valeurs de la dernière ligne. Pour ce faire on crée une liste temporaire qui correspondra à la ligne suivante. On la complète en utilisant les valeurs de la ligne actuelle. Cette ligne étant complète, on remplace `ligne_maxis` par cette nouvelle ligne.

    {{ IDE('somme_liste.py')}}
