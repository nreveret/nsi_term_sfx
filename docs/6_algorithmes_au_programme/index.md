---
title : Algorithmes au programme de NSI
---


Ce document regroupe différents algorithmes au programme de NSI. Il n'a pas la prétention d'être exhaustif.

On considère dans tout ce document qu'un tableau de longueur `n` a des indices allant de `0` à `n-1`.

Les classes de complexités citées concernent toutes la complexité en temps. On rappelle à ce titre quelques exemples de classes de complexités ainsi qu'une illustration de leurs "vitesses" relatives.

<center>

|          Temps          |          Type de complexité          | Temps pour $n = 5$ | Temps pour $n = 10$ | Temps pour $n = 10 000$ |               Exemple de problème                |
| :---------------------: | :----------------------------------: | :----------------: | :-----------------: | :---------------------: | :----------------------------------------------: |
|    $\mathcal{O}(1)$     |         complexité constante         |       10 ns        |        10 ns        |          10 ns          |          Accès à une cellule de tableau          |
| $\mathcal{O}(\log{n})$  |       complexité logarithmique       |       10 ns        |        10 ns        |          40 ns          |              Recherche dichotomique              |
| $\mathcal{O}(\sqrt{n})$ |         complexité racinaire         |       22 ns        |        32 ns        |          1 µs           |              Test de primalité naïf              |
|    $\mathcal{O}(n)$     |         complexité linéaire          |       50 ns        |       100 ns        |         100 µs          |                Parcours de liste                 |
| $\mathcal{O}(n\log{n})$ |      complexité linéarithmique       |       40 ns        |       100 ns        |         400 µs          |                    Tri fusion                    |
|   $\mathcal{O}(n^2)$    | complexité quadratique (polynomiale) |       250 ns       |        1 µs         |           1 s           |         Tris par insertion et sélection          |
|   $\mathcal{O}(n^3)$    |   complexité cubique (polynomiale)   |      1.25 µs       |        10 µs        |       2.7 heures        |         Multiplication matricielle naïve         |
|   $\mathcal{O}(2^n)$    |       complexité exponentielle       |       320 ns       |        10 µs        |           ...           |      Problème du sac à dos par force brute       |
|    $\mathcal{O}(n!)$    |        complexité factorielle        |       1.2 µs       |        36 ms        |           ...           | Problème du voyageur de commerce par force brute |

</center>

!["Vitesses" des différentes classes de complexité](complexite.jpg){width=60% .center .autolight}

On propose ci-dessous des algorithmes en langage naturel. Sous chacun d'eux un éditeur Python permet de transcrire cet algorithme.

## Recherche dans un tableau

??? info "Recherche du minimum/maximum"

    On se donne un tableau `tab`. Quel est l'indice de l'élément minimal ? Et le maximal ? Il faut lire l'ensemble des valeurs pour le déterminer : la complexité est linéaire $\mathcal{O}(n)$ ($n$ est la taille du tableau).

    ```
    Fonction minimum(tab : tableau) -> Entier :
        # Initialisation
        indice_mini = 0

        # Parcours des valeurs restantes à partir de la 1-ième
        Pour i allant de 1 à longueur(tab) - 1 (inclus) :
                Si tab[i] < tab[indice_mini] :
                    indice_mini = i

        Renvoyer indice_mini
    ```
    
    ??? example "Éditeur"
    
        {{ IDE('minimum')}}

??? info "Recherche linéaire d'un élément"

    On se donne un tableau de taille `n` et un valeur `cible`. Cette valeur est-elle dans le tableau ? Si oui on renvoie son indice, sinon on provoque une erreur.

    Dans cette version, on effectue un parcours linéaire. Dans le cas où la valeur est située à la fin du tableau, il faut lire toutes les valeurs : la complexité est linéaire $\mathcal{O}(n)$ ($n$ est la taille du tableau).

    On peut adapter cet algorithme afin qu'il renvoie l'indice d'un élément respectant une certaine condition en modifiant le test portant sur `tab[i]`.

    ```
    Fonction recherche_linéaire(tab : tableau, cible : valeur) -> Entier :
        Pour i allant de 1 à longueur(tab) - 1 (inclus) :
            Si tab[i] == cible :
                Renvoyer i

        Provoquer une erreur
    ```

    ??? example "Éditeur"
    
        {{ IDE('lineaire')}}
        
## Recherche dichotomique

On se donne un tableau de taille `n` **trié** et un valeur `cible`. Cette valeur est-elle dans le tableau ? Si oui on renvoie son indice, sinon on provoque une erreur.

Dans cette version, on utilise un méthode dichotomique :

* Tant que la zone de recherche a une longueur strictement positive :
    * on compare la valeur à l'élément du milieu de la zone de recherche :
        * s'il est supérieur, on cherche dans la zone de gauche,
        * s'il est inférieur on cherche dans la zone de droite,
        * s'il est égal à la valeur on renvoie son indice,

* On provoque une erreur.

On tire profit du fait que le tableau est déjà trié. Comme on partage la zone de recherche en $2$ à chaque étape, l'algorithme fera autant de comparaison qu'il est possible de couper la taille du tableau en $2$ jusqu'à obtenir $1$. La complexité esr donc **logarithmique** $\mathcal{O}(\log_2{n})$ ($n$ est la taille du tableau).

On propose ci-dessous deux versions de cette recherche dichotomique : itérative ou récursive.

??? info "Version itérative"

    ```
    Fonction dichotomie_iter(tab : tableau, cible : valeur) -> Entier :
        # La zone de recherche
        debut = 0
        fin = longueur(tab) -1

        # Cas général, tant que la zone de recherche comprend plus de 2 valeurs
        Tant que debut < fin :
            # le milieu
            milieu = (debut + fin) // 2 # quotient entier
            Si cible < tab[milieu] :
                fin = milieu - 1
            Sinon si tab[milieu] < cible :
                debut = milieu + 1
            Sinon :
                Renvoyer milieu

        Provoquer une erreur
    ```

    ??? example "Éditeur"
    
        {{ IDE('dicho_iter')}}
        

??? info "Version récursive"

    ```
    Fonction dichotomie_rec(tab : tableau, debut : entier, fin : entier, cible : valeur) -> Entier :
        # l'élément d'indice debut est inclus
        # l'élément d'indice fin est exclu
        
        # Cas de base : la zone de recherche ne contient qu'un seul élément
        Si fin - debut < 1 :
            Provoquer une erreur

        # Cas général
        # le milieu
        milieu = (debut + fin) // 2 # quotient entier
        Si cible < tab[milieu] :
            Renvoyer Dichotomie_re(tab, debut, milieu, cible)
        Sinon si tab[milieu] < cible :
            Renvoyer Dichotomie_re(tab, milieu + 1, fin, cible)
        Sinon :
            Renvoyer milieu
    ```
    
    ??? example "Éditeur"
    
        {{ IDE('dicho_rec')}}
        

## Algorithmes de tri

???+ note "Cours détaillés"

    Des cours détaillés sur les tris présentés ci-dessous sont disponibles [sur cette page](https://nreveret.forge.apps.education.fr/tris/).

??? info "Tri par insertion"

    Cette méthode de tri consiste à parcourir l'ensemble du tableau (à partir du deuxième élément) et à faire « remonter » chaque élément jusqu'à ce qu'il ait atteint le début du tableau ou qu'il ne soit précédé que par des éléments qui lui sont inférieurs. C'est le tri utilisé par beaucoup de personnes pour trier des cartes.

    <center>
    ![Tri de cartespar insertion](insertion_cards_Cormen.png){width=30% .autolight style="vertical-align :top;margin-top :5%; margin-right :5%"}
    ![Tri par insertion](insertion_sort.jpg){width=30% .autolight}
    </center>

    La complexité de cet algorithme est **quadratique** $\mathcal{O}(n^2)$ ($n$ est la taille du tableau).

    Il s'agit d'un tri en place (les éléments sont triés directement dans le tableau de départ) et stable (deux éléments qui étaient déjà classés dans le tableau de départ ressortent dans le même ordre).

    ```title="Langage naturel"
    Fonction tri_insertion(tab : tableau) -> Rien :
        Pour i allant de 1 à longueur(tab) - 1 (inclus) :
            a_inserer = tab[i]
            j = i
            Tant que j > 0 et tab[j - 1] < a_inserer :
                tab[j] = tab[j - 1]
                j = j - 1
            tab[j] = a_inserer
    ```

    ??? example "Éditeur"
    
        {{ IDE('insertion')}}
        

??? info "Tri par sélection"

    Cette méthode de tri consiste à parcourir l'ensemble du tableau (jusqu'à l'avant-denier élément) et, lors de chaque itération, à sélectionner le plus petit élément situé à droite de l'élément atteint. Ce minimum alors échangé avec l'élément en cours d'étude.

    ![Tri par sélection](selection_sort.jpg){width=40% .center .autolight}

    La complexité de cet algorithme est là-encore **quadratique** $\mathcal{O}(n^2)$ ($n$ est la taille du tableau).

    Il s'agit d'un tri en place et stable.

    ```
    Fonction tri_sélection(tab : tableau) -> Rien :
        Pour i allant de 0 à longueur(tab) - 2 (inclus) :
            indice_mini = i
            # Recherche de l'indice du minimum
            Pour j allant de i à longueur(tab) - 1 (inclus) :
                Si tab[j] < tab[indice_mini] :
                    indice_mini = j
            Échanger tab[i] et tab[indice_mini]
        
    ```

    ??? example "Éditeur"
    
        {{ IDE('selection')}}
        

??? info "Tri fusion"

    Il s'agit d'un tri utilisant la méthode *Diviser Pour Régner*.

    Le principe est de couper récursivement le tableau en deux jusqu'à obtenir des tableaux de taille $1$. Une fois ceux-ci atteints, on fusionne ces tableaux (qui sont de fait déjà triés) en piochant à chaque étape de plus petit élément de chaque tableau.

    ![Tri fusion](merge_sort.png){width=35% .center .autolight}

    Cet algorithme est donc de complexité **linéarithmique** $\mathcal{O}(n\log{n})$ ($n$ est la taille du tableau).

    C'est un tri stable mais pas en place (dans la version présentée ici).
    
    Cette méthode est construite autour de deux fonctions :

    * `fusion` : fusionne les deux sous-tableaux passés en argument ;

    * `tri_fusion` : partage le tableau en deux sous-tableaux. C'est la fonction récursive.

    ```
    Fonction fusion(gauche : tableau, droite : tableau) -> tableau :
        # Le tableau qui sera renvoyé
        resultat = [Rien pour i allant de 0 à longueur(gauche) + longueur(droite) (exclu)]

        # les indices de lecture et l'indice d'écriture
        i_gauche = 0
        i_droite = 0
        i_resultat = 0

        Tant que i_gauche < longueur(gauche) et i_droite < longueur(droite) :
            Si gauche[0] <= droite[0] :
                resultat[i_resultat] = gauche[0]
                i_gauche = i_gauche + 1
            Sinon :
                resultat[i_resultat] = droite[0]
                i_droite = i_droite + 1
            i_resultat = i_resultat + 1
            
        Tant que i_gauche < longueur(gauche):
            resultat[i_resultat] = gauche[0]
            i_gauche = i_gauche + 1
            i_resultat = i_resultat + 1

        Tant que i_droite < longueur(droite) :
            resultat[i_resultat] = droite[0]
            i_droite = i_droite + 1
            i_resultat = i_resultat + 1

        Renvoyer resultat
    
    Fonction tri_fusion(tab : tableau) -> tableau :
        # Cas de base
        Si longueur(tab) == 1 :
            Renvoyer tab
        
        # Cas général
        # L'indice du "milieu" du tableau
        milieu = longueur(tab)// 2 # quotient entier

        # Les sous-tableaux de gauche et de droite
        gauche = [tab[i] pour chaque i entre 0 et milieu (exclu)]
        droite = [tab[i] pour chaque i entre milieu et longueur(tab) (exclu)]

        # Tri de gauche et droite
        gauche = tri_fusion(gauche)
        droite = tri_fusion(droite)

        # On renvoie leur fusion
        Renvoyer fusion(gauche, droite)
    ```

    ??? example "Éditeur"
    
        {{ IDE('fusion')}}

## Algorithmes gloutons

Les algorithmes **gloutons** sont une famille d'algorithmes permettant de résoudre des problèmes d'optimisation. Il s'agit de déterminer la solution optimale (la meilleure) à un problème.

Les algorithmes gloutons interviennent souvent lorsque la détermination immédiate de la solution optimale ne peut pas être faite et lorsque l'étude de toutes les solutions possibles est trop longue.

Dans ce cas, on utilise une méthode itérative qui détermine petit à petit une solution **satisfaisante**, à défaut d'être **optimale**. A chaque étape on fait le meilleur choix **à cet instant**. C'est donc une méthode *vorace*, *greedy* en anglais. L'expression *algorithme glouton* est d'ailleurs une traduction de *greedy algorithm*.

Il est courant qu'une algorithme glouton débute par trier des données. La complexité de ces algorithmes est donc **linéarithmique** $\mathcal{O}(n\log{n})$ ($n$ est la taille du tableau de données trié).

??? info "Rendu de monnaie"

    Illustrons cette méthode par le problème du rendu de monnaie. Il s'agit de rendre une somme en utilisant un nombre minimum de pièces.

    On considère un jeu de pièces pour lequel la méthode gloutonne renvoie la solution optimale. Un tel jeu de pièces est dit *canonique*.
    
    On considère que le jeu de pièces est déjà trié dans l'ordre décroisant.

    ```
    Fonction rendu_monnaie(somme : entier, pièces : tableau d'entiers) -> tableau d'entiers :
        résultat est un tableau vide
        i = 0
        Tant que somme > 0 :
            valeur = pièces[i]
            Si valeur est inférieur ou égal à somme :
                Ajouter valeur à résultat
            Sinon :
                i = i + 1
        Renvoyer résultat
    ```
    
    ??? example "Éditeur"
    
        {{ IDE('monnaie')}}

## Algorithmes sur les arbres binaires

!!! note "Cours détaillé"
    
    Un cours détaillé sur les arbres binaires est disponible [sur cette page](../2_tad/6_arbres_binaires/index.md).

??? note "Arbres enracinés ?"

    On présente ci-dessous les algorithmes sur les *arbres binaires*.
    
    Les algorithmes analogues sur les [*arbres enracinés*](../2_tad/5_arbres/index.md) (possédant un nombre quelconque d'enfants) sont similaires à deux nuances près :
    
    * le cas de base des appels récursifs est le cas où l'arbre est une *feuille* ;

    * on parcourt l'ensemble des enfants et pas seulement les sous-arbres gauche et droit.

Un arbre binaire est soit :

* un arbre vide ;

* un arbre qui possède exactement deux enfants (appelés sous-arbre gauche et sous-arbre droit), qui sont eux-mêmes des arbres (potentiellement vides).

??? info "Hauteur d'un arbre binaire"

    On peut calculer la hauteur d'un arbre en utilisant une fonction récursive.

    ```
    Fonction hauteur(arbre : arbre binaire) -> entier :
        Si arbre est vide :
            Retourner 0
        Sinon :
            Retourner 1 + max(hauteur(sous-arbre gauche, hauteur(sous-arbre droit))
    ```

    ??? example "Éditeur"

        {{ IDE('hauteur_ab')}}

??? info "Taille d'un arbre binaire"

    On rappelle que la taille d'un arbre est égale au nombre de nœuds non-vides qu'il contient. Là encore il est possible de déterminer la taille à l'aide d'une fonction récursive.

    ```
    Fonction taille(arbre : arbre binaire) -> entier :
        Si arbre est vide :
            Retourner 0
        Sinon :
            Retourner 1 + taille(sous-arbre gauche) + taille(sous-arbre droit)
    ```

    ??? example "Éditeur"

        {{ IDE('taille_ab')}}

??? info "Parcours en largeur"

    <center>
    
    ```mermaid
    flowchart TD
        A(11)
        B(8)
        C(14)
        D(5)
        E(10)
        F(13)
        G(15)
        
        A --- B
        A --- C
        B --- D
        B --- E
        C --- F
        C --- G
    ```
    </center>

    Sur la figure, le parcours en largeur donnera l'ordre suivant (on convient que les enfants de gauche sont visités avant ceux de droite) :

    <center>11 → 8 → 14 → 5 → 10 → 13 → 15</center>

    On peut implémenter un parcours en largeur à l'aide d'une *file* :

    ```
    Fonction largeur(arbre : arbre binaire) -> Tableau de valeurs :
        f est une file vide
        Enfiler arbre dans f
        résultat est un tableau vide
        
        # Parcours proprement dit
        Tant que f est non vide
            Défiler un arbre a de f
            Si a est non vide :
                Ajouter a à la fin de résultat
                Enfiler le sous-arbre gauche de a dans f
                Enfiler le sous-arbre droit de a dans f
    ```

    ??? example "Éditeur"

        {{ IDE('largeur_ab')}}
        
??? info "Parcours en profondeur"

    <center>
    
    ```mermaid
    flowchart TD
        A(11)
        B(8)
        C(14)
        D(5)
        E(10)
        F(13)
        G(15)
        
        A --- B
        A --- C
        B --- D
        B --- E
        C --- F
        C --- G
    ```
    </center>

    On distingue trois parcours en profondeur des arbres binaires :
    
    * les parcours *préfixe* dans lesquels le nœud est traité avant ses enfants : 11 → 8 → 5 → 10 → 14 → 13 → 15;

    * les parcours *infixe* dans lesquels le nœud est traité après l'enfant gauche mais avant le droit : 5 → 8 → 10 → 11 → 13 → 14 → 15;

    * et les parcours *postfixe* dans lequel le nœud est traité après ses enfants: 5 → 10 → 8 → 13 → 15 → 14 → 11.

    On utilise dans chacun des cas une fonction récursive. La différence entre les trois parcours se traduit dans le code dans l'ordre de traitement des enfants et du nœud en cours.

    ```
    Fonction préfixe(arbre : arbre binaire) -> Tableau de valeurs :
        Si arbre est vide:
            Renvoyer le tableau vide
        
        résultat est un tableau vide
        Ajouter la valeur de arbre à la fin de résultat
        Ajouter préfixe(sous-arbre gauche) à la fin de résultat
        Ajouter préfixe(sous-arbre droit) à la fin de résultat
        
        Renvoyer résultat
    
    Fonction infixe(arbre : arbre binaire) -> Tableau de valeurs :
        Si arbre est vide:
            Renvoyer le tableau vide
        
        résultat est un tableau vide
        Ajouter infixe(sous-arbre gauche) à la fin de résultat
        Ajouter la valeur de arbre à la fin de résultat
        Ajouter infixe(sous-arbre droit) à la fin de résultat
        
        Renvoyer résultat
    
    Fonction suffixe(arbre : arbre binaire) -> Tableau de valeurs :
        Si arbre est vide:
            Renvoyer le tableau vide
        
        résultat est un tableau vide
        Ajouter suffixe(sous-arbre gauche) à la fin de résultat
        Ajouter suffixe(sous-arbre droit) à la fin de résultat
        Ajouter la valeur de arbre à la fin de résultat
        
        Renvoyer résultat
    ```

    ??? example "Éditeur"

        {{ IDE('profondeur_ab')}}


??? info "Insertion dans un arbre binaire de recherche"

    Afin d'insérer une valeur `x` dans un ABR non vide, on procède récursivement jusqu'à atteindre un nœud vide bien positionné.
    
    ```
    Fonction insertion_abr(arbre : arbre binaire de recherche, x : valeur) -> Rien:
        Si x est strictement inférieur à l'étiquette de la racine :
            Si le sous-arbre gauche est vide :
                Le sous-arbre gauche devient un ABR dont la racine a pour étiquette x
            Sinon :
                insertion_abr(sous-arbre gauche, x)
        Sinon si x est strictement supérieur à l'étiquette de la racine :
            Si le sous-arbre droit est vide :
                Le sous-arbre droit devient un ABR dont la racine a pour étiquette x
            Sinon :
                insertion_abr(sous-arbre droit, x)
    ```
    
    ??? example "Éditeur"

        {{ IDE('insertion_abr')}}

??? info "Recherche dans un arbre binaire de recherche"

    Afin de rechercher une valeur `x` dans un ABR, on procède récursivement jusqu'à atteindre un nœud de valeur égale ou un arbre vide.
    
    ```
    Fonction recherche_abr(arbre : arbre binaire de recherche, x : valeur) -> booléen:
        Si arbre est vide :
            Renvoyer Faux
            
        Si x est strictement inférieur à l'étiquette de la racine :
            Renvoyer recherche_abr(sous-arbre gauche, x)
        Sinon si x est strictement supérieur à l'étiquette de la racine :
            Renvoyer recherche_abr(sous-arbre droit, x)
        Sinon :
            Renvoyer Vrai
    ```
    
    ??? example "Éditeur"

        {{ IDE('recherche_abr')}}
        
## Algorithmes sur les graphes

!!! note "Cours détaillé"

    Un cours détaillé sur les graphes est disponible [sur cette page](../2_tad/4_graphes/index.md).

??? info "Parcours en largeur"

    !!! note "Plus court chemin"
    
        Dans le cas d'un graphe non pondéré, le parcours en largeur permet de déterminer le plus court chemin entre deux sommets.
        
        Si le graphe est pondéré, un approche possible est de lister l'ensemble des chemins et de comparer leurs poids.

    On retrouve un algorithme proche de celui rencontré avec les arbres à cette différence qu'il faut ici garder trace des sommets déjà rencontrés afin de ne pas « tomber » dans un cycle.

    <center>

    ```mermaid
    flowchart LR
    A(A) --- B(B)
    A --- D(D)
    A --- C(C)
    B --- E(E)
    C --- D
    B --- D
    D --- E
    ```
    </center>

    Le parcours en largeur issu de A visite dans l'ordre : A → B → C → D → E.

    ```
    Fonction largeur(graphe : graphe, depart : sommet) -> Tableau de valeurs:

        f est une File vide
        Enfiler depart dans f
        
        visité est un dictionnaire vide
        Pour chaque sommet de graphe:
            visité[sommet] = Faux
    
        résultat est un tableau vide

        Tant que f est non vide :
            Défiler le sommet actuel de f
            Ajouter actuel à la fin de résultat
            visité[actuel] = Vrai
            Pour chaque voisin de actuel :
                Si visité[voisin] est Faux :
                    Enfiler voisin dans f
    ```
    
    ??? example "Éditeur"
    
        {{ IDE('largeur_graphe')}}
        
??? info "Parcours en profondeur"

    <center>

    ```mermaid
    flowchart LR
    A(A) --- B(B)
    A --- D(D)
    A --- C(C)
    B --- E(E)
    C --- D
    B --- D
    D --- E
    ```
    </center>

    Le parcours en profondeur issu de A visite dans l'ordre : A → B → D → C → E.

    Pour le parcours en profondeur, on peut utiliser une fonction récursive ou une approche itérative à l'aide d'une *pile*. On propose ici la version récursive. Le dictionnaire des sommets `visités` est passé en paramètre.

    ```
    Fonction profondeur(graphe : graphe, depart : sommet, visités: dictionnaire) -> Tableau de valeurs :
        résultat est un tableau vide
        Ajouter départ à la fin de résultat
        visités[départ] = Vrai
        Pour chaque voisin de actuel :
            Si visité[voisin] est Faux :
                Ajouter profondeur(graphe, voisin, visités) à la fin de résultat
        Renvoyer résultat
    ```
    
    ??? example "Éditeur"
    
        {{ IDE('profondeur_graphe')}}

??? info "Présence de cycles"

    On peut facilement repérer l'existence de cycles dans un graphe en utilisant un parcours en profondeur : si l'on rencontre un sommet déjà visité (autre que le sommet d'origine du déplacement ayant mené au sommet en cours d'étude), c'est qu'il existe un cycle.
    
    On propose un parcours en profondeur à l'aide d'une pile. A chaque étape, on empile le couple `(sommet à visiter, sommet d'origine)`.

    ```
    Fonction est_cyclique(graphe : graphe) -> booléen :
        Pour chaque sommet départ de graphe:
            p est une Pile vide
            visités est un dictionnaire vide
            Pour chaque sommet de graphe :
                visités[sommet] = Faux
            Empiler le couple (départ, None) dans p

            Tant que p est non vide :
                Dépiler le couple (actuel, origine) de p
                visités[actuel] = Varai
                Pour chaque voisin de actuel :
                    Si voisin est différent de origine:
                        Si visités[voisin]:
                            Renvoyer Vrai
                        Sinon :
                            Empiler le couple (voisin, actuel) dans p
            
            Renvoyer Faux
    ```
    
        
    ??? example "Éditeur"
    
        {{ IDE('cycle')}}


## Programmation dynamique

!!! note "Cours détaillé"
    
    Un cours détaillé sur cette méthode est disponible [sur cette page](../4_algorithmique/2_prog_dyn/index.md).

La programmation dynamique est une méthode de programmation permettant de résoudre un problème en résolvant préalablement certains problèmes équivalents de taille inférieure. Cette méthode s'apparente donc à *Diviser Pour Régner*. Toutefois dans la programmation dynamique, on prend soin de conserver les résultats des calculs intermédiaires afin de ne pas les calculer à nouveau.

Considérons le problème consistant à calculer le $n$-ième terme de la suite de Fibonacci $(F_n)$ définie ci-dessous :

$$F_0 = 0$$

$$F_1 = 1$$

$$\forall n \geqslant 2,\,F_n=F_{n-1}+F_{n-2}$$

??? info "Suite de Fibonacci descendante (avec mémoïsation)"

    Le calcul récursif du $n$-ième terme de la suite de Fibonacci fait intervenir à plusieurs reprises les mêmes appels. Par exemple $F_5=F_4+F_3=(F3+F_2)+(F_2+F_1)$.
    
    On peut alléger la démarche en gardant trace des calculs déjà effectués. Cette méthode est appelée *mémoïsation*.
    
    On utilise une approche récursive (descendante).

    ```
    mémoire est un dictionnaire
    
    Fonction fibonacci_rec(n : entier) -> entier :
        Si n <= 1 :
            Renvoyer n

        Si n n'est pas une clé de mémoire :
            mémoire[n] = fibonacci_rec(n - 1) + fibonacci_rec(n - 2)
        Renvoyer mémoire[n]
    ```

    ??? example "Éditeur"
    
        {{ IDE('fibo_rec')}}

??? info "Suite de Fibonacci ascendante"

    Il est aussi possible d'utiliser une approche itérative, ascendante.

    ```
    Fonction fibonnacci_iter(n : entier) -> entier :
        F est un tableau initialisé à [0, 1]
        Tant que longueur(F) < n + 1 :
            dernier = F[longueur(F) - 1]
            avant_dernier = F[longueur(F) - 2] 
            Ajouter (dernier + avant_dernier) à la fin de F
        Renvoyer F[n]
    ```
    
    ??? example "Éditeur"
    
        {{ IDE('fibo_iter')}}


## *k* plus proches voisins


L'algorithme des $k$ Plus Proches Voisins, *$k$ Nearest Neighbors* en anglais (*KNN*), fait partie des algorithmes de classification.

Le but de cet algorithme est d'attribuer une catégorie (une classe) à un individu. On se donne pour cela une base de données d'individus pour lesquels on connaît :

* des descripteurs numériques,

* la catégorie (qualitative ou quantitative).

Seules les valeurs des descripteurs sont connues pour l'individu « mystère ».

Le principe est illustré sur la figure ci-dessous :

![Algorithme des $k$ plus proches voisins](knn_algo.png){width=50% .center .autolight}

La démarche est donc la suivante :

* On calcule toutes les distances entre l'individu « mystère » et les individus de la base ;

* On trie la base dans l'ordre croissant des distances à l'individu « mystère » ;

* On observe les $k$ premiers voisins : la catégorie la plus représentée parmi ceux-ci est attribuée à l'individu « mystère ».

Dans l'algorithme ci-dessous on considère que le tableau `voisins` contient la liste non vide des catégories des voisins triée par distance au point « mystère » croissante. Ainsi, le premier élément de `voisin` est la catégorie du point le plus proche.

`k` est le nombre de voisins à prendre en considération. On suppose que `k` est inférieur ou égal au nombre de voisins.

```
Fonction knn(voisins : tableau de catégories, k : entier) -> catégorie :
    effectif est un dictionnaire vide
    
    catégorie_maxi = voisins[0]
    effectif_maxi = 1
    Pour i allant de 1 à k (exclu):
        catégorie = voisins[i]
        Si catégorie est une clé de effectif :
            effectif[catégorie] = effectif[catégorie] + 1
        Sinon :
            effectif[catégorie] = 1
        Si effectif[catégorie] > effectif_maxi:
            effectif_maxi = effectif[catégorie]
            catégorie_maxi = catégorie
    
    Renvoyer catégorie_maxi
```

??? example "Éditeur"

    {{ IDE('knn')}}

   
## Recherche textuelle

??? info "Algorithme de Boyer-Moore"

    !!! note "Cours détaillé"
    
        Un cours détaillé sur cet algorithme est disponible [sur cette page](../4_algorithmique/3_recherche_textuelle/index.md).

    Cet algorithme a été développé par Robert S. Boyer et J. Strother Moore en 1977.

    Le point de départ est plutôt contre-intuitif : pour chercher un motif dans un texte, on commence par la dernière lettre du motif ! En effet, si la lettre du texte correspondante n'est pas la bonne, il est inutile de tester les autres lettres. Cela ne change pas beaucoup par rapport à l'approche naïve.

    La vraie différence se situe dans l'action qui suit la comparaison des caractères :

    * Si la lettre observée ne fait même pas partie du motif, on peut alors décaler la fenêtre de lecture directement de la longueur du motif. On évite ainsi de faire autant de tests que le motif est long.

    * Que se passe-t-il lorsque la lettre du texte correspond à celle du motif ? On étudie alors les lettres précédentes jusqu'à avoir soit :
        * fini le motif (et donc trouvé une correspondance parfaite),
        * soit trouvé un mauvais caractère.

    * Dernier cas de figure : le caractère lu n'est pas correct mais il apparaît toutefois à une autre position dans le motif. Dans ce cas on doit décaler la fenêtre de lecture afin de faire correspondre ce caractère et celui du motif situé le plus à droite (la dernière occurrence de la lettre).

    * Il peut toutefois arriver que la dernière occurrence du caractère cherché soit située plus loin dans le motif : le saut ferait alors reculer la fenêtre de lecture ce qui est inefficace. Dans ce cas, on se contente de faire un saut de $1$ caractère. C'est ce qui se passe entre les étapes 2 et 3.

    L'algorithme de Boyer-Moore nécessite donc avant d'effectuer les comparaisons de savoir quels sont les décalages à effectuer. Pour ce faire il doit construire un dictionnaire `décalages` reprenant l'indice dans le motif de la dernière occurrence de chacun de ses caractères.

    La fonction suivante crée ce dictionnaire :

    ```
    Fonction creation_décalage(motif : chaîne de caractères) -> dictionnaire[caractère, entier] :
        dico est un dictionnaire vide

        Pour chaque entier i entre 0 et la longueur du motif :
            dico[motif[i]] prend la valeur i            

        Renvoyer décalage
    ```

    On crée un dictionnaire dont les entrées sont les lettres du motif. On parcourt de la gauche vers la droite en ajoutant ou mettant à jour le décalage associé à chaque caractère.

    L'algorithme de Boyer-Moore peut alors être donné :

    ```
    Fonction recherche_BMH(texte : chaîne de caractères, motif : chaîne de caractères) -> liste :
        N est la longueur du texte
        P est la longueur du motif

        Si P > N :
            Renvoyer une erreur  # le motif est trop long

        décalages prend la valeur creation_décalage(motif)

        i prend la valeur 0
        Tant que i est strictement inférieur à N - P + 1 :
            j prend la valeur P - 1 :
            Tant que j est positif et que motif[j] est égal à texte[i + j] :
                Décrémenter j
            Si j vaut -1 :
                Renvoyer i  # Succès
            Si texte[i + j] est dans décalages :
                Augmenter i du maximum entre 1 et P - 1 - décalages[motif[j]]
            Sinon :
                Augmenter i de P

        Renvoyer une erreur  # le motif n'est pas présent
    ```
    
    ??? example "Éditeur"
    
        {{ IDE('boyer')}}