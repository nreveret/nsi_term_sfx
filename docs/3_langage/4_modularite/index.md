---
title: Modularité
---

## Les modules

L'un des points forts de Python est sa licence libre et la possibilité offerte de créer des modules (bibliothèques) facilement.

Il est facile de connaître les modules installés sur votre machine. Pour ce faire, il suffit de saisir :

* (Windows) : `pip list` ;

* (Windows, si la précédente ne fonctionne pas) : `python -m pip list` ;

* (Linux et MacOS) : `pip3 list` ;

* (Linux et MacOS, si la précédente ne fonctionne pas) : `python3 -m pip3 list`.

``` title="Terminal"
toto@ordi-toto:~$ pip3 list
Package                          Version
-------------------------------- -----------
argon2-cffi                      21.3.0
argon2-cffi-bindings             21.2.0
Arpeggio                         2.0.2
astroid                          2.9.3
asttokens                        2.0.5
attrs                            23.2.0
autopep8                         1.6.0
...
```


## Installer un module

La façon la plus simple d'installer un module Python est de passer par un terminal.

Imaginons que l'on souhaite installer le module [`pandas`](https://pandas.pydata.org/) (utile pour les traitements de données), selon notre plateforme on saisira (dans un terminal) :

* (Windows) : `pip install pandas` ;

* (Windows, si la précédente ne fonctionne pas) : `python -m pip install pandas` ;

* (Linux et MacOS) : `pip3 install pandas` ;

* (Linux et MacOS, si la précédente ne fonctionne pas) : `python3 -m pip3 install pandas`

## Importer un module

Il existe deux façon d'utiliser un module dans un programme Python.

???+ info "Import de base"

    On saisit `import <nom_du_module>` afin d'indiquer que l'on utilisera les fonctions du module indiqué.
    
    Chaque référence à une fonction se fera sous la forme `<nom_du_module>.<fonction>`.
    
    **Bien que plus lourde lors des appels de fonctions, cette méthode est à privilégier car elle permet de garder trace de l'origine de la fonction utilisée.
    
???+ info "Import d'une fonction particulière"

    On saisit `from <nom_du_module> import <fonction>` afin d'indiquer que l'on utilisera la fonction indiquée du module indiqué.
    
    Chaque référence à cette fonction se fera sous la forme `<fonction>`.


??? example "Exemples"

    {{ IDE('imports.py') }}


## Créer un module

Un module python permet de regrouper des variables, fonctions, classes... que l'on souhaite utiliser dans plusieurs programmes. Lors de l'importation, Python *lit* le fichier et stocke donc en mémoire les objets rencontrés.

Observons la définition de la fonction `random.choice` de python : [code source de python](https://github.com/python/cpython/blob/main/Lib/random.py).

```python
def choice(self, seq):
    """Choose a random element from a non-empty sequence."""
    try:
        i = self._randbelow(len(seq))
    except ValueError:
        raise IndexError('Cannot choose from an empty sequence') from None
    return seq[i]
```

On observe :

* une documentation entre les triples guillemets ;

* une vérification que la liste est non vide. Un erreur est levée si c'est le cas ;

* un traitement.
* 
### La documentation

Il existe de nombreuses façons de rédiger des [documentations](http://sametmax.com/les-docstrings/).

Retenons qu'une bonne documentation doit avoir :

* une description générale de la fonction ;

* une description des arguments de la fonction (types, rôle) ;

* une description de la valeur retournée par la fonction.

??? example "Fonction `moyenne`"

    On définit ci-dessous la fonction `moyenne`.

    {{ IDE('moyenne.py')}}

### Les tests / préconditions

Lorsque l'on tape une fonction, on doit s'assurer de sa correction :

* le fait que la fonction se termine (terminaison) ...

* ... et qu'elle effectue les bons calculs sans créer d'erreur.

Une façon de s'assurer que la fonction ne va pas créer d'erreur est de tester les arguments.

??? info "Précondition avec `assert`"

    On peut utiliser différentes façon de faire dont le `assert` :

    `assert <condition>, <Message si la condition n'est pas respectée>`

    Une précondition non respectée provoquera alors une `AssertionError`.

    On définit ci-dessous la fonction `aire_disque` qui calcule... l'aire d'un disque !

    {{ IDE('aire_disque.py')}}


??? info "Précondition avec `if`"

    Il est aussi possible de provoquer d'autres types d'erreurs en procédant ainsi : `#!py if not condition: raise <erreur>`.

    On définit ci-dessous à nouveau la fonction `aire_disque`.

    {{ IDE('aire_disque_2.py')}}

### Les tests

Bien qu'il soit impossible de tester la totalité des entrées possibles, il est bon de munir un module de tests bien choisis.

Ceux-ci doivent :

* couvrir les cas de bases ;

* intercepter les erreurs possibles ;

* gérer les cas particuliers.

??? example "Tests de `aire_disque`"

    {{ IDE('aire_disque_3.py')}}

???+ note "Code non exécuté"

    Le test `#!py if __name__ == "__main__":` permet de n'exécuter les lignes qui suivent que si le module exécuté est celui-ci.
    
    Ces lignes seront ignorées en cas d'import de ce fichier.