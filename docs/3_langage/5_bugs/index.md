---
title: Gestion des bugs
---

Avant de recenser des erreurs classiques, commençons par apprendre à les « lire ».

## Anatomie d'une exception Python

Commençons par générer un « bug » en exécutant le code ci-dessous :

{{ IDE('bug_1.py') }}

Commençons par la fin. On trouve sur la dernière ligne deux informations :

* le type d'erreur (ici `TypeError`) ;
* un message expliquant plus précisément l'erreur.

Les lignes précédentes constituent le *traceback*. Il faut le lire de bas en haut :

* la dernière ligne indique l'instruction qui a provoqué cette erreur,
* les lignes du dessus remontent la « généalogie » de cette erreur.

Ici on lit donc (de bas en haut):

| Sortie dans la console                                   | Commentaire                                                                           |
| :------------------------------------------------------- | :------------------------------------------------------------------------------------ |
| `Traceback (most recent call last):`                     |                                                                                       |
| `  File "<exec>", line 7, in <module>`                   | L'appel qui a lancé toute la démarche était à la ligne 7, dans le corps du programme. |
| `  File "<exec>", line 5, in f`                          | L'appel du dessus a été effectué à la ligne 5, dans la fonction `f`                   |
| `  File "<exec>", line 2, in ajoute_5`                   | L'erreur a été provoquée par l'instruction à la ligne 2, dans la fonction `ajoute_5`  |
| `TypeError: can only concatenate str (not "int") to str` | L'erreur est une `TypeError` car on cherche à concaténer un `str` et un `int`         |


## Les différentes erreurs

Cette section est bien entendue non exhaustive ! On propose en commentaire sous chaque code, la version corrigée.

??? info "`IndentationError`"

    En Python, les blocs doivent être indentés (avec 4 espaces le plus souvent).

    {{ IDE('indentation.py')}}

    L'erreur vient du fait que le sous-bloc (à l'intérieur du `if`) doit être indenté, décalé. L'usage veut que l'on utilise soit quatre espaces soit une tabulation.

??? info "`SyntaxError`"

    Il s'agit le plus souvent d'une faute de frappe ou d'une omission (de parenthèse fermante par exemple).
    
    {{ IDE('syntax.py')}}

    On a omis le `:` à la fin de la première ligne.

??? info "`NameError`"

    Là encore, il s'agit le plus souvent d'une faute de frappe.
    
    {{ IDE('name.py')}}

    La fonction s'appelle `toto`, pas `tata` !

??? info "`ImportError`"

    On cherche à importer un module qui n'existe pas.
    
    {{ IDE('import.py')}}

??? info "`IndexError`"

    Archi-classique : on cherche à accéder à une valeur qui n'existe pas dans une liste.
    
    {{ IDE('index.py')}}

??? info "`TypeError`"

    On cherche à utiliser un objet d'un type non pris en charge.
    
    {{ IDE('type.py')}}
    

## Erreurs de conception de programme

Dans certains cas, un programme n'effectue pas ce que l'on souhaite sans que Python ne détecte d'erreur. Il est donc plus compliqué de trouver la source du problème. Voici quelques cas à envisager.

??? info "Conditions non exhaustives"

    On ne couvre pas tous les cas de figure...
    
    {{ IDE('condition.py')}}

    Ici, on ne gère pas les bonnes notes !


??? info "Effet de bord entrant"

    Une fonction dépend d'une variable accessible à l'extérieur d'elle-même.
    
    {{ IDE('effet_bord_entrant.py')}}
    
    Pour s'en sortir, on passe en paramètres **toutes** les informations utiles.
    
??? info "Effet de bord sortant"

    Une fonction  modifie une variable à l'extérieur d'elle-même.
    
    {{ IDE('effet_bord_sortant.py')}}
    
    On peut soit renvoyer la valeur modifiée, soit prendre soin de ne pas modifier les entrées !

??? info "Égalité de flottant"

    En informatique, les nombres à virgule flottante peuvent être représentés de façon approchée. On ne doit jamais tester leur égalité.
    
    {{ IDE('flottants.py')}}