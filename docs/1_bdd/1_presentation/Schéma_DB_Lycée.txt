//// SUR https://dbdiagram.io/d
//// SUR https://dbdiagram.io/d
//// -- LEVEL 1
//// -- Tables and References

// Creating tables
Table eleves {
  id int [pk, increment] // auto-increment
  nom varchar
  prenom varchar
  naissance date
  classe int
  spe_1 int
  spe_2 int
}

Table classes {
  id int [pk, increment]
  nom varchar
  prof_principal int
 }
 
Table matieres {
   id int [pk, increment]
   nom varchar
 }
 
Table cours {
   id int [pk, increment]
   matiere int
   professeur int
 }
 
Table professeurs {
  id int [pk, increment]
  nom varchar
 }


// Creating references
// You can also define relaionship separately
// > many-to-one; < one-to-many; - one-to-one
Ref: eleves.classe < classes.id  
Ref: eleves.spe_1 < cours.id
Ref: eleves.spe_2 < cours.id
Ref: cours.matiere < matieres.id
Ref: cours.professeur < professeurs.id
Ref: classes.prof_principal - professeurs.id


