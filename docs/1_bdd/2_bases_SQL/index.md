---
title: Bases de SQL
---


???+ note "Site dédié"

    Vous trouverez un cours sur le SQL à [cette adresse](https://nreveret.forge.apps.education.fr/exercices_bdd/).
    
Il existe plusieurs langages permettant d'interagir avec une base de données relationnelle. Le plus utilisé est le *Structured Query Language* ou **SQL**.

Ce langage peut être partagé en trois parties :

* *la définition des données* : pour créer et modifier des tables et lignes dans la base ;

* *le contrôle de transaction* : pour interroger la base par exemple ;

* *le contrôle des données* : pour gérer l'accès aux données (selon le statut de l'utilisateur par exemple) .

On utilise ci-dessous du SQL classique. On pourra à ce propos consulter avec intérêt les sites :

* [https://nreveret.forge.apps.education.fr/exercices_bdd/memento_sql/](https://nreveret.forge.apps.education.fr/exercices_bdd/memento_sql/)
* [https://sql.sh/](https://sql.sh/)
* [https://www.w3schools.com/SQl/](https://www.w3schools.com/SQl/)

???+ note "SQLite"

    *SQLite*, que l'on utilise souvent avec Python, est un dialecte de SQL qui diffère sur certains points, secondaires pour nous. On pourra consulter [ce site](https://sqlite.org/index.html).

Nous allons dans cette partie utiliser le schéma relationnel suivant :

![Schéma relationnel](Schema_DB_Lycee.png){width=65% .center .autolight}

Nous pouvons donc créer une base de données `lycee` :

```sql
CREATE DATABASE IF NOT EXISTS lycee;
```

???+ note "Présentation des codes"

    On choisir de présenter les codes en sautant des lignes à certains endroit afin de bien distinguer les différentes parties des instructions.
    
    Ce n'est pas indispensable.
    
    De la même façon, les `;` à la fin de chaque instruction sont indispensables dans certains dialectes SQL mais pas dans d'autres. Dans le doute, on les utilise à chaque fois.

## Création des tables

La structure de base pour créer une table est la suivante :

```sql
CREATE TABLE nom_de_la_table
(
    colonne1 type_donnees,
    colonne2 type_donnees,
    colonne3 type_donnees,
);
```

Il est aussi possible d'ajouter des contraintes, pour les clés primaires et étrangères par exemples.

Ci-dessous est donnée le code créant la table `eleves` :

```sql
CREATE TABLE eleves 
(
    id INTEGER NOT NULL AUTO_INCREMENT,
    nom VARCHAR(100),
    prenom VARCHAR(100),
    naissance VARCHAR(100),
    classe INTEGER,
    spe_1 INTEGER,
    spe_2 INTEGER,

    PRIMARY KEY (id)

    FOREIGN KEY (classe) REFERENCES classes(id),
    FOREIGN KEY (spe_1) REFERENCES cours(id),
    FOREIGN KEY (spe_2) REFERENCES cours(id)
);
```

Le code est assez clair :

* on crée tout d'abord la table en précisant son nom
* puis les variables en indiquant leur types. `VARCHAR(100)` signifie que l'on utilise du texte comptant au maximum 100 caractères
* vient ensuite la clé primaire...
* ... et enfin les clés secondaires


## Insérer des valeurs

Les tables étant créées il est intéressant de les peupler en insérant des lignes (*row* en anglais)

La fonction à utiliser est `INSERT` et sa structure :

```sql
INSERT INTO TABLE_NAME (column1, column2, column3,...columnN)
VALUES (value1, value2, value3,...valueN);
```

Dans bien des cas, il n'est pas nécessaire de fournir la valeur de la clé primaire si celle-ci est actualisée automatiquement

Par exemple : 

```sql
INSERT INTO eleves (nom, prenom, naissance, classe, spe_1, spe_2)
VALUES ("Dupont", "Eric", "01/03/03", 1, 5, 2);
```

On peut aussi insérer plusieurs lignes à la fois (les lignes sont séparées par des virgules):

```sql
INSERT INTO eleves (nom, prenom, naissance, classe, spe_1, spe_2)
VALUES 
("Dupont", "Eric", "01/03/03", 1, 5, 2),
("Durand", "Elea", "10/03/04", 2, 3, 1);
```

## Rechercher des valeurs

### Sélection de base

Une fois la base créée et peuplée, l'action la plus courante est de sélectionner des données.

La fonction à utiliser est `SELECT` et sa structure :

```sql
SELECT nom_du_champ
FROM nom_du_tableau
WHERE condition;
```

* On peut par exemple rechercher les informations de l'élève dont l'`id` est 8 :

```sql
SELECT * 
FROM eleves
WHERE id = 8;
```

Le `*` (appelé *wildcard*) signifie que l'on sélectionne toutes les colonnes.

* On peut aussi indiquer le nom des attributs à sélectionner :

```sql
SELECT nom, prenom
FROM eleves
WHERE id = 8;
```

* Et si l'on souhaite obtenir **toutes** les informations de **tous** les élèves ? On omet la condition !

```sql
SELECT * 
FROM eleves;
```

* Pour les conditions multiples ?

```sql
SELECT nom
FROM eleves
WHERE id <= 16 AND classe = 2;
```

On sélectionne ainsi les élèves dont l'`id` est inférieur ou égal à `16` et la `classe` est la `2`. On peut utiliser les opérateurs booléens `AND`, `OR` et `NOT`.

* Comment sélectionner les chaînes de caractères ?

```sql
SELECT nom
FROM eleves
WHERE prenom = "Julien";
```

permet de sélectionner les `Julien`.

Par contre :

```sql
SELECT nom
FROM eleves
WHERE prenom LIKE "Jul%";
```
permettra de sélectionner les élèves dont le prénom commence par `Jul` (les Julie, Julia, Julio, Julien...). Dans le même esprit :

* `WHERE prenom LIKE "%a";` permet de sélectionner les prénoms se terminant par `a` (Clara, Paula...);
* `WHERE prenom LIKE "%ul%";` permet de sélectionner les prénoms contenant la chaîne `ul` (Jules, Paul...);


### Jointures

Imaginons que nous souhaitions sélectionner les élèves de la "Terminale 1"... Nous pourrions faire une requête du type 

```sql
SELECT nom, prenom
FROM eleves
WHERE classe = 1;
```

mais il faudrait être certain que l'identifiant de la "Terminale 1" est bien le numéro 1... Or voici la table `classes` :

<center>

| id  | classe | prof_principal |
| --- | ------ | -------------- |
| 1   | 2nde-1 | M. Ayrault     |
| 2   | 2nde-2 | M. Beyrault    |
| ... | ...    | ...            |
| 15  | Term-1 | M. Heyrault    |
| ... | ...    | ...            |

</center>

La "Terminale 1" n'a pas l'`id` numéro 1 !

Si l'on consulte le schéma proposé plus haut, on constate que la `classe` de chaque élève correspond à un `id` de la table `classe`. Il est ainsi possible de préciser la classe en mettant en correspondance les tables `eleves` et `classes`. Comme nous souhaitons sélectionner les élèves pour lesquels la classe est **exactement** la "Terminale 1" et pas plus, nous allons faire une `JOIN`. La figure ci-dessous mentionne `INNER JOIN` qui est équivalent au simple `JOIN`.

![Jointures](joins.png){width=50% .center .autolight}

La structure est la suivante :

```sql
SELECT *
FROM table1
JOIN table2 ON table1.id = table2.fk_id
WHERE condition;
```

Dans le cas présent on fait :

```sql
SELECT *
FROM eleves
JOIN classes ON eleves.classe = classes.id
WHERE classes.nom = "Term-1";
```

Expliquons les choses :

* `SELECT * FROM eleves` : on sélectionne tous les attributs de la table `eleves` ...
* `JOIN classes` : ... en indiquant que l'on souhaite utiliser des critères de la table `classes`...
* `ON eleves.classe = classes.id` : ... en rappelant la relation entre les tables (la `classe` de l'élève correspond à l'`id` de la classe )...
* `WHERE classes.nom = "Term-1"` :... et on ne prend que les lignes pour lesquelles le `nom` de la classe est `Term-1` 

### Présentation des résultats

* Il est possible dans une recherche de compter les résultats :

```sql
SELECT count(*)
FROM eleves;
```
On obtient avec cette requête le nombre d'élèves du lycée.


* Afin de renommer un résultat, on utilise un *alias* :

```sql
SELECT count(*) as nombre
FROM eleves;
```

Là encore on compte le nombre d'élèves mais ce décompte est renvoyé dans la colonne `nombre`.

* Pour les tri, on utilise les mots clés `ORDER BY ... ASC` (ordre croissant) ou `ORDER BY ... DESC` (ordre décroissant) :

```sql
SELECT nom
FROM eleves
ORDER BY nom ASC;
```

Cette requête renvoie les noms de tous les élèves triés dans l'ordre alphabétique

* Enfin, il est possible de grouper les données par exemple pour compter le nombre d'élèves par classe. On utilise le mot clé `GROUP BY ...`

```sql
SELECT classes.nom, count(eleves.id) as nombre_eleves
FROM eleves
JOIN classes ON eleves.classe = classes.id
GROUP BY classes.nom;
```

On a ainsi :

* sélectionné le nom de chaque classe et compté combien elle comporte d'élèves (en comptant les `id`)
* en réalisant une jointure avec la table `classes`
* et en groupant les résultats selon les `classes.nom`

## Modifier des valeurs

### Mettre à jour une valeur

Comment faire si un élève change de classe ? On met à jour sa donnée avec `UPDATE` :

```sql
UPDATE table_name
SET column1 = value1, column2 = value2, ...
WHERE condition; 
```

Cela peut donner (en connaissant bien les `id` des élèves et des classes)

```sql
UPDATE eleves
SET eleves.classes = 3
WHERE eleves.id = 8;
```

En étant astucieux, on peut ainsi renommer tous les élèves :

```sql
UPDATE eleves
SET eleves.nom = "Wayne";
```

### Supprimer une entrée

On a parfois besoin de supprimer une entrée/ligne. On utilise :

```sql
DELETE FROM table_name
WHERE condition; 
```

Ainsi :

```sql
DELETE FROM eleves
WHERE eleves.id = 9;
```

permet d'effacer l'élève d'`id` numéro 9 s'il quitte le lycée.

![https://xkcd.com/327/](https://imgs.xkcd.com/comics/exploits_of_a_mom.png){width=60% .center .autolight}