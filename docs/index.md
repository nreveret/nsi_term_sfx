---
title: Cours de Terminale NSI de N. Revéret
author: N. Revéret
---


Vous trouverez sur ce site les cours de la Spécialité *Numérique et Sciences Informatiques* rédigés par [Nicolas Revéret](https://forge.apps.education.fr/nreveret).

Le programme de l'année de Terminale est [téléchargeable ici](spe247_annexe_1158933.pdf){:download="spe247_annexe_1158933.pdf"}.

Vous trouvez utiliser le sommaire ci-contre afin de naviguer dans les différents cours.

Vous trouverez aussi un [bac à sable](bac_a_sable.md) vous permettant de saisir et évaluer du code en ligne.

Enfin, le site [Codex](https://codex.forge.apps.education.fr/) vous propose de nombreux exercices de programmation en Python sur divers points de ce cours.