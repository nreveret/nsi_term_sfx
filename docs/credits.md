---
title: 👏 Crédits
---


Ce site a été rédigé par N. Revéret (`nreveret@ac-rennes.fr)`.

L'ensemble des contenus est sous [licence CC BY-NC-SA 4.0](http://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1){ target="_blank}.

Il est donc **possible** de *partager* et *modifier* ce contenu.

Vous êtes par contre **tenus** de *créditer* les auteurs, de *ne pas faire un usage commercial* du contenu et de le partager *sous la même licence*.

---------------------------

Le site utilise les technologies suivantes (entre autres) :

* [**mkdocs**](https://www.mkdocs.org/) : génération du site `html` à partir de fichiers `markdown` ;

* [**mkdocs-material**](https://squidfunk.github.io/mkdocs-material/) : thème de `mkdocs`;
* [**pyodide**](https://pyodide.org/en/stable/) : portage de **Python** dans le navigateur ;
* [Thème `pyodide-mkdocs-theme`](https://gitlab.com/frederic-zinelli/pyodide-mkdocs-theme) développé par Frédéric Zinelli, basé sur le travail [Vincent Bouillot](hhttps://gitlab.com/bouillotvincent/pyodide-mkdocs).

Le logo du site provient de <a href="https://www.flaticon.com/free-icons/distance-learning" title="distance-learning icons">Distance-learning icons created by Vector Squad - Flaticon</a>
